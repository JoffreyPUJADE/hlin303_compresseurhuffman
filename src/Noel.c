#include "../include/Noel.h"

#include <stdio.h>
#include <string.h>

int estPair(int nb)
{
	return ((nb % 2) == 0);
}

void dessinerPartieSapin(int nombreLignes, int sautALaLigne)
{
	if(!estPair(nombreLignes))
		return;
	
	definirCouleurConsole(VERT);
	
	for(int i=0;i<nombreLignes;++i)
	{
		for(int j=0;j<(nombreLignes-i);++j)
			printf(" ");
		
		for(int j=0;j<((i*2)-1);++j)
			printf("*");
		
		if(sautALaLigne == 1)
			printf(((i < (nombreLignes - 1)) ? "\n" : ""));
		else
			printf("\n");
	}
	
	definirCouleurConsole(NORMAL);
}

void dessinerPartieTronc(int largeurSapin, int hauteurTronc)
{
	if(!estPair(largeurSapin))
		return;
	
	definirCouleurConsole(MAGENTA);
	
	int milieu = (largeurSapin - 1);
	
	for(int i=0;i<hauteurTronc;++i)
	{
		for(int j=0;j<(milieu - 1);++j)
			printf(" ");
		
		printf("@@@\n");
	}
	
	definirCouleurConsole(NORMAL);
}

void dessinerSapin()
{
	dessinerPartieSapin(10, 1);
	dessinerPartieSapin(10, 0);
	
	dessinerPartieTronc(10, 5);
}

void dessinBonnesFetes()
{
	// Merci au site : http://patorjk.com/software/taag/#p=display&f=Bolger&t=Bonnes%20fetes
	// Pour cette très belle police d'écriture. Police utilisée sur le site : Bolger.
	definirCouleurConsole(ROUGE);
	
	printf("888~~\\                                                      88~\\             d8                    \n");
	printf("888   |  e88~-_  888-~88e 888-~88e  e88~~8e   d88~\\       _888__  e88~~8e  _d88__  e88~~8e   d88~\\ \n");
	printf("888 _/  d888   i 888  888 888  888 d888  88b C888          888   d888  88b  888   d888  88b C888   \n");
	printf("888  \\  8888   | 888  888 888  888 8888__888  Y88b         888   8888__888  888   8888__888  Y88b  \n");
	printf("888   | Y888   ' 888  888 888  888 Y888    ,   888D        888   Y888    ,  888   Y888    ,   888D \n");
	printf("888__/   \"88_-~  888  888 888  888  \"88___/  \\_88P         888    \"88___/   \"88_/  \"88___/  \\_88P  \n");
	
	printf("\n\n\n\n");
	
	printf(" /~~88b   ,88~~\\    /~~88b   ,88~~\\   \n");
	printf("|   888  d888   \\  |   888  d888   \\  \n");
	printf("`  d88P 88888    | `  d88P 88888    | \n");
	printf("  d88P  88888    |   d88P  88888    | \n");
	printf(" d88P    Y888   /   d88P    Y888   /  \n");
	printf("d88P___   `88__/   d88P___   `88__/  \n");
	
	definirCouleurConsole(NORMAL);
}

void messageDesDeveloppeurs()
{
	definirCouleurConsole(CYAN);
	
	printf("Chers utilisateurs,\n");
	printf("cette ann%se 2020 f%st tr%ss particuli%sre.\n", eAccAigu, uAccCirconflexe, eAccGrave, eAccGrave);
	printf("En effet, le SARS-Cov-2 a touch%s le monde entier, %s tel point que l'on parle aujourd'hui d'une pand%smie. Mais cette\n", eAccAigu, aAccGrave, eAccAigu);
	printf("ann%se est sur le point de se terminer. Et cette fin d'ann%se 2020 am%sne avec elle son lot de festivit%s habituel. Et ce,\n", eAccAigu, eAccAigu, eAccGrave, eAccAigu);
	printf("m%sme avec les difficult%ss que nous traversons. Nous vous souhaitons donc de passer d'excellentes f%stes de fin d'ann%se\n", eAccCirconflexe, eAccAigu, eAccCirconflexe, eAccAigu);
	printf("2020. Prot%sgez-vous, prot%sgez vos proches, mais r%sunissez-vous avec votre entourage. Et que l'ann%se 2021 soit parsem%se\n", eAccAigu, eAccAigu, eAccAigu, eAccAigu, eAccAigu);
	printf("de r%sussite, de sant%s et de bonheur.\n", eAccAigu, eAccAigu);
	printf("Cordialement,\n");
	printf("Joffrey PUJADE et Tom TISSERANT.\n");
	
	definirCouleurConsole(NORMAL);
}

void etMaintenantUnMessageDesDeveloppeurs()
{
	dessinerSapin();
	
	printf("\n\n\n");
	
	messageDesDeveloppeurs();
	
	printf("\n\n\n");
	
	dessinBonnesFetes();
}
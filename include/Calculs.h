#ifndef CALCULS_H
#define CALCULS_H

/*! \page page11 Calculs
 * \par Description du fichier \a "Calculs.h" \a 
 * 		Le fichier \a "Calculs.h" \a contient le nécessaire pour réaliser le calcul de la probabilité d'apparition des
 * 		caractères dans le fichier source.
 * 		
 * 		Il contient la procédure ::calculProba(char *chemin, float *proba).
 */

/*! \file Calculs.h
 * \brief Contient tout le nécessaire calculer.
 * \details Ce fichier contient le nécessaire pour réaliser le calcul de la probabilité d'apparition des caractères dans
 * 			le fichier source.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "Arbre.h"

/*! \fn void calculProba(char *chemin, float *proba)
 * \brief Calcule les probabilités des caractères.
 * \param [in] eth (EnTeteHuffman const*) : En-tête du fichier.
 * \details Cette procédure permet de calculer la probabilité d'apparition de chaque caractère du fichier source.
 * \callergraph
 * \callgraph
 */
void calculProba(EnTeteHuffman const* eth);

#endif // CALCULS_H
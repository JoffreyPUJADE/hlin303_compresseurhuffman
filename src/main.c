#include "stdio.h"
#include "../include/Inclusions.h"

#include <stdlib.h>

void aide()
{
	printf("SYNOPSIS\n");
	printf("	Ce programme a %st%s con%su par Joffrey PUJADE et Tom TISSERANT. Il s'agit d'un compresseur et d%scompresseur de Huffman.\n", eAccAigu, eAccAigu, cCedille, eAccAigu);
	printf("OPTIONS\n");
	printf("	\"-n\" ou \"--noel\"\n");
	printf("		Une fonctionnalit%s surprise %s l'occasion des f%stes de fin d'ann%se 2020.\n", eAccAigu, aAccGrave, eAccCirconflexe, eAccAigu);
	printf("	\"-c\" ou \"--compresser\" [nomFichierSource] [nomFichierCompresse]\n");
	printf("		Fonctionnalit%s de compression de Huffman. [nomFichierSource] est le fichier %s compresser et [nomFichierCompresse] est le fichier de destination.\n", eAccAigu, aAccGrave);
	printf("	\"-d\" ou \"--decompresser\" [nomFichierCompresse]\n");
	printf("		Fonctionnalit%s de d%scompression de Huffman. [nomFichierCompresse] est le fichier %s d%scompresser.\n", eAccAigu, eAccAigu, aAccGrave, eAccAigu);
	printf("	\"-h\" ou \"--help\"\n");
	printf("		Affiche cette aide.\n");
}

void compressionDeHuffman(char* nomFichierSource, char* nomFichierCompresse) // Fonction pour compresser un fichier ; elle est temporairement placée dans le main
{
	if(estVide(nomFichierSource))
	{
		definirCouleurConsole(ROUGE);
		
		fprintf(stderr, "ERREUR : Le fichier source \"%s\" est vide. Compression du fichier impossible.", nomFichierSource);
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	Arbre* a = creerArbre(nomFichierSource);
	
	EnTeteHuffman* eth = creerEnTeteHuffman(nomFichierSource, a);
	
	eth->nombreBitsCompresse = 0;
	
	compressionHuffman(nomFichierSource, nomFichierCompresse, a, eth);
	
	if(a->tabNoeuds != NULL)
		free(a->tabNoeuds);
	if(a != NULL)
		free(a);
	
	//if(eth->nomOriginalFichier != NULL)
	//	free(eth->nomOriginalFichier);
	
	if(eth->tabCorrespondance != NULL)
	{
		for(int i=0;i<eth->tabCorrespondance[0].nombreFeuilles;++i)
		{
			if(eth->tabCorrespondance[i].code != NULL)
				free(eth->tabCorrespondance[i].code);
		}
		
		free(eth->tabCorrespondance);
	}
	
	if(eth != NULL)
		free(eth);
	
	free(nomFichierCompresse);
	free(nomFichierSource);
}

void decompressionDeHuffman(char* nomFichierCompresse)
{
	if(estVide(nomFichierCompresse))
	{
		definirCouleurConsole(ROUGE);
		
		fprintf(stderr, "ERREUR : Le fichier compress%s \"%s\" est vide. D%sompression du fichier impossible.", eAccAigu, nomFichierCompresse, eAccAigu);
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	int taille = 0;
	uchar* fichierCompresse = fichierVersChaine(nomFichierCompresse, &taille); // Lecture du fichier et stockage dans une chaîne
	EnTeteHuffman* eth = lireEnTeteHuffman(fichierCompresse); // Création de l'en-tête via la chaîne contenant le fichier compressé
	Arbre* a = creerArbreDepuisEnTeteHuffman(eth); // Création d'un arbre de Huffman depuis l'en-tête créée ; inutile pour le moment donc désalloué après création
	
	decompressionHuffman(fichierCompresse, a, ((char*)eth->nomOriginalFichier));
	
	free(eth->nomOriginalFichier);
	free(eth->tabCorrespondance);
	free(eth);
	
	free(nomFichierCompresse);
}

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		definirCouleurConsole(ROUGE);
		
		fprintf(stderr, "ERREUR : aucun argument n'a %st%s donn%s.", eAccAigu, eAccAigu, eAccAigu);
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	if((strcmp(argv[1], "-n") == 0) || (strcmp(argv[1], "--noel") == 0))
	{
		etMaintenantUnMessageDesDeveloppeurs();
		
		return 0;
	}
	
	if((strcmp(argv[1], "-c") == 0) || (strcmp(argv[1], "--compresser") == 0))
	{
		if(argc < 4)
		{
			definirCouleurConsole(ROUGE);
			
			fprintf(stderr, "ERREUR : Il manque le fichier %s compresser (fichier source) et le nom du fichier compress%s (fichier destination).", aAccGrave, eAccAigu);
			
			definirCouleurConsole(NORMAL);
			
			exit(1);
		}
		
		char* nomFichierSource = malloc(sizeof(char) * strlen(argv[2]));
		strcpy(nomFichierSource, argv[2]);
		
		char* nomFichierCompresse = malloc(sizeof(char) * strlen(argv[3]));
		strcpy(nomFichierCompresse, argv[3]);
		
		compressionDeHuffman(nomFichierSource, nomFichierCompresse);
		
		printf("fin\n");
		
		return 0;
	}
	else if((strcmp(argv[1], "-d") == 0) || (strcmp(argv[1], "--decompresser") == 0))
	{
		if(argc < 3)
		{
			definirCouleurConsole(ROUGE);
			
			fprintf(stderr, "ERREUR : Il manque le nom du fichier compress%s.", eAccAigu);
			
			definirCouleurConsole(NORMAL);
			
			exit(1);
		}
		
		char* nomFichierCompresse = malloc(sizeof(char) * strlen(argv[2]));
		strcpy(nomFichierCompresse, argv[2]);
		
		decompressionDeHuffman(nomFichierCompresse);
		
		printf("fin\n");
		
		return 0;
	}
	else if((strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "--help") == 0))
	{
		aide();
		
		printf("fin\n");
		
		return 0;
	}
	
	definirCouleurConsole(ROUGE);
	
	fprintf(stderr, "ERREUR : Argument invalide. Utilisez l'argument \"-h\" ou \"--help\" pour obtenir de l'aide %s l'utilisation.", aAccGrave);
	
	definirCouleurConsole(NORMAL);
	
	return 1;
}
#ifndef NOEUD_H
#define NOEUD_H

/*! \page page8 Noeud
 * \par Description du fichier \a "Noeud.h" \a 
 * 		Le fichier \a "Noeud.h" \a contient le nécessaire pour représenter un Noeud de Huffman.
 * 		
 * 		Il contient les alias de types suivants :
 * 			- ::uchar ;
 * 			- ::uint ;
 * 			- ::Noeud.
 * 		
 * 		Il contient également la strucutre ::Noeud.
 */

/*! \file Noeud.h
 * \brief Contient tout le nécessaire pour représenter un Noeud de Huffman.
 * \details Ce fichier contient les alias de type "uchar", "uint" et Noeud" ainsi que la structure "Noeud",
 * 			représentant un Noeud de Huffman.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

/*! \typedef unsigned char uchar;
 * \brief Alias de type.
 * \details Cet alias définit le type "uchar" à utiliser en lieu et place du type "unsigned char".
 */
typedef unsigned char uchar;

/*! \typedef unsigned int uint;
 * \brief Alias de type.
 * \details Cet alias définit le type "uint" à utiliser en lieu et place du type "unsigned int".
 */
typedef unsigned int uint;

/*! \struct Noeud
 * \brief Structure d'un noeud.
 * \details Cette structure définit ce que contient un noeud dans un arbre de Huffman. Ici, un noeud est caractérisé par deux
 *  		valeurs et trois indices reliant ce noeud au reste de l'arbre.
 */
struct Noeud
{
	int nombreOccurences; /*!< Cette variable contient le nombre d'occurences d'un caractère dans un fichier. */
	uchar c; /*!< Cette variable contient le caractère du noeud. */
	int gauche; /*!< Cette variable contient l'indice du fils gauche du noeud. */
	int droit; /*!< Cette variable contient l'indice du fils droit du noeud. */
	int parent; /*!< Cette variable contient l'indice du parent du noeud. */ // Indice du parent.
};

/*! \typedef struct Noeud Noeud;
 * \brief Alias de structure.
 * \details Cet alias définit le type "Noeud" à utiliser en lieu et place du type "struct Noeud".
 */
typedef struct Noeud Noeud;

#endif // NOEUD_H
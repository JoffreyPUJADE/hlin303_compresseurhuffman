#ifndef MACROSCMDOS_H
#define MACROSCMDOS_H

/*! \page page5 MacrosCmdOs
 * \par Description du fichier \a "MacrosCmdOs.h" \a 
 * 		Le fichier \a "MacrosCmdOs.h" \a définit différentes macros pour exécuter des commandes système sous Windows et
 * 		sous les systèmes Posix (Linux, de préférence.
 * 		
 * 		Les macros définies dans ce fichier sont :
 * 			- DEL : macro contenant la commande de suppression d'un fichier (Windows/Linux/MacOS) ;
 * 			- LIST : macro contenant la commande pour lister les fichiers d'un répertoire (Windows/Linux/MacOS) ;
 * 			- DELETEAFILEFORME( file ) : macro permettant la suppression d'un fichier (Windows/Linux/MacOS).
 * 		
 * \warning ATTENTION : La macro DELETEAFILEFORME doit être utilisée UNIQUEMENT UNE SEULE FOIS dans une même portée ;
 * 						le fait est qu'elle définie une ou plusieurs variables selon le langage utilisé, donc, si cette
 * 						macro est utilisée deux fois dans une même portée, il y a un fort risque de déclarations multiples ("multiple declarations") !
 */

/*! \file MacrosCmdOs.h
 * \brief Contient tout le nécessaire pour exécuter des commandes spécifiques.
 * \details Ce fichier définit différentes macros pour exécuter des commandes système sous Windows et
 * 			sous les systèmes Posix (Linux, de préférence. Les macros définies dans ce fichier sont :
 * 				- DEL : macro contenant la commande de suppression d'un fichier (Windows/Linux/MacOS) ;
 * 				- LIST : macro contenant la commande pour lister les fichiers d'un répertoire (Windows/Linux/MacOS) ;
 * 				- DELETEAFILEFORME( file ) : macro permettant la suppression d'un fichier (Windows/Linux/MacOS).
 * \warning ATTENTION : La macro DELETEAFILEFORME doit être utilisée UNIQUEMENT UNE SEULE FOIS dans une même portée ;
 * 						le fait est qu'elle définie une ou plusieurs variables selon le langage utilisé, donc, si cette
 * 						macro est utilisée deux fois dans une même portée, il y a un fort risque de déclarations multiples ("multiple declarations") !
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "MacrosOS.h"
#include "MacrosLangage.h"
 
// Définition des macros pour les fonctions DOS/Shell.
#if defined (WINDOWS)
	#define DEL "del" /*!< Cette macro définit la fonction DOS pour supprimer un fichier. */
	#define LIST "dir /b /s" /*!< Cette macro définit la fonction DOS pour lister des fichiers. */
#elif defined (POSIX) || defined (MACOS)
	#define DEL "rm -f" /*!< Cette macro définit la fonction Bash pour supprimer un fichier. */
	#define LIST "ls -a" /*!< Cette macro définit la fonction Bash pour lister des fichiers. */
#endif

// Définition d'une macro pour supprimer un fichier selon les systèmes d'exploitation.
#if defined(CPP_LANGUAGE)
	#include <sstream>
	#include <cstdlib>
	
	/*! \brief Supprime un fichier d'un répertoire.
	 * \param file : Fichier à supprimer.
	 * \details Cette macro, ici définie pour le C++, permet de supprimer un fichier de l'ordinateur.
	 * \callergraph
	 * \callgraph
	 */
	#define DELETEAFILEFORME( file ) \
			std::ostringstream ossDelFile; \
			ossDelFile << DEL << " " << file; \
			system(ossDelFile.str().c_str());
#elif defined(C_LANGUAGE)
	#include <string.h>
	#include <stdlib.h>
	
	/*! \brief Supprime un fichier d'un répertoire.
	 * \param file : Fichier à supprimer.
	 * \details Cette macro, ici définie pour le C, permet de supprimer un fichier de l'ordinateur.
	 * \callergraph
	 * \callgraph
	 */
	#define DELETEAFILEFORME( file ) \
			char* delFile = malloc(sizeof(char)); \
			strcpy(delFile, DEL); \
			strcat(delFile, " "); \
			strcat(delFile, file); \
			system(delFile);
#endif

#endif // MACROSCMDOS_H
#include "../include/Huffman.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

// --- Manipulation du binaire ---

uchar obtenirLeIemeBit(uchar c, int i)
{
     return ((c>>i) & 1);
}

uchar* caractereVersBinaire(uchar ch)
{
	uchar* res = (uchar*)malloc((sizeof(uchar) * 9));
	
	sprintf(
		((char*)res),
		"%d%d%d%d%d%d%d%d",
		obtenirLeIemeBit(ch, 7),
		obtenirLeIemeBit(ch, 6),
		obtenirLeIemeBit(ch, 5),
		obtenirLeIemeBit(ch, 4),
		obtenirLeIemeBit(ch, 3),
		obtenirLeIemeBit(ch, 2),
		obtenirLeIemeBit(ch, 1),
		obtenirLeIemeBit(ch, 0)
	);
	
	return res;
}

void affichageBinaireDUnCaractere(uchar ch)
{
	printf("%d", obtenirLeIemeBit(ch, 7));
	printf("%d", obtenirLeIemeBit(ch, 6));
	printf("%d", obtenirLeIemeBit(ch, 5));
	printf("%d", obtenirLeIemeBit(ch, 4));
	printf("%d", obtenirLeIemeBit(ch, 3));
	printf("%d", obtenirLeIemeBit(ch, 2));
	printf("%d", obtenirLeIemeBit(ch, 1));
	printf("%d", obtenirLeIemeBit(ch, 0));
}

// --- Compression ---

void chiffresHuffman(Arbre* a, EnTeteHuffman const* eth, int nbBitsCompresses)
{
	definirCouleurConsole(JAUNE);
	printf("\n===================================================\n");
	definirCouleurConsole(MAGENTA);
	printf("Probabilit%ss d'apparition des caract%sres :\n", eAccAigu, eAccGrave);
	definirCouleurConsole(NORMAL);
	
	calculProba(eth);
	
	definirCouleurConsole(JAUNE);
	printf("\n===================================================\n");
	definirCouleurConsole(MAGENTA);
	printf("Arbre de Huffman :\n");
	definirCouleurConsole(NORMAL);
	
	for(int i=0;i<a->taille;++i)
	{
		printf("i %d : gauche (indice) : %d, droit (indice) : %d, parent (indice) : %d, nbOcc: %d, c : %c\n\n",
				i,
				a->tabNoeuds[i].gauche,
				a->tabNoeuds[i].droit,
				a->tabNoeuds[i].parent,
				a->tabNoeuds[i].nombreOccurences,
				a->tabNoeuds[i].c);
	}
	
	definirCouleurConsole(JAUNE);
	printf("\n===================================================\n");
	definirCouleurConsole(MAGENTA);
	printf("En-t%ste de Huffman :\n", eAccCirconflexe);
	definirCouleurConsole(NORMAL);
	
	afficherEnTeteHuffman(eth);
	
	definirCouleurConsole(JAUNE);
	printf("\n===================================================\n");
	definirCouleurConsole(MAGENTA);
	printf("Longueur moyenne de codage :\n");
	definirCouleurConsole(NORMAL);
	
	int incLongueurCodeTemp = 0;
	int nbLettresTemp = 0;
	float resTemp = 0.0;
	
	for(int i=0;i<eth->tabCorrespondance[0].nombreFeuilles;++i)
	{
		incLongueurCodeTemp += strlen((char*)eth->tabCorrespondance[i].code);
		++nbLettresTemp;
	}
	
	resTemp = (incLongueurCodeTemp / nbLettresTemp);
	
	printf("La longueur moyenne de codage est : %f .\n", resTemp);
	
	definirCouleurConsole(JAUNE);
	printf("\n===================================================\n");
	definirCouleurConsole(MAGENTA);
	printf("Taux de compression :\n");
	definirCouleurConsole(NORMAL);
	
	int tailleOriginelleSource = (eth->tailleOriginaleFichier * 8);
	int tailleCompressee = nbBitsCompresses;
	float tauxDeCompression = (1.0 - (((float)tailleCompressee) / ((float)tailleOriginelleSource)));
	
	printf("Taille originelle : %d; taille compress%se : %d; gain : %f %% !\n", tailleOriginelleSource, eAccAigu, tailleCompressee, tauxDeCompression);
	
	definirCouleurConsole(JAUNE);
	printf("\n===================================================\n");
	definirCouleurConsole(NORMAL);
}

void compressionHuffman(char const* nomFichierSource, char const* nomFichierDestination, Arbre* a, EnTeteHuffman const* eth)
{
	FILE* fichierSource = NULL;
	fichierSource = fopen(nomFichierSource, "r+");
	FILE* fichierDestination = NULL;
	fichierDestination = fopen(nomFichierDestination, "w");
	
	if(fichierSource == NULL)
	{
		fprintf(stderr, "ERREUR : Impossible de lire le fichier \"%s\" en mode lecture.\nFonction \"%s\".\n", nomFichierSource, FUNCTION_NAME);
		
		exit(1);
	}
	
	if(fichierDestination == NULL)
	{
		fprintf(stderr, "ERREUR : Impossible de lire le fichier \"%s\" en mode lecture.\nFonction \"%s\".\n", nomFichierDestination, FUNCTION_NAME);
		
		exit(1);
	}
	
	ecrireEnTeteHuffman(fichierDestination, eth);
	
	uchar* chaine = (uchar*)malloc((sizeof(uchar) * 8));
	chaine[0] = '\0';
	
	int cmptBitsCompresses = 0;
	
	uchar c;
	
	while(!feof(fichierSource))
	{
		size_t valeurTampon = fread(&c, sizeof(char), 1, fichierSource);
		++valeurTampon;
		
		if(ferror(fichierSource))
			printf("erreur lecture\n");
		
		if(((int)c) == 24)
		{
			definirCouleurConsole(ROUGE);
			
			fprintf(stderr, "ERREUR : Caract%sre de contr%sle d%stect%s dans le fichier source :\n", eAccGrave, oAccCirconflexe, eAccAigu, eAccAigu);
			fprintf(stderr, "[CAN] position ASCII : 24\n");
			fprintf(stderr, "N'utilisez aucun caract%sre de contr%sle dans les fichiers source.\n", eAccGrave, oAccCirconflexe);
			
			definirCouleurConsole(NORMAL);
			
			exit(1);
		}
		else if(((int)c) == 26)
		{
			definirCouleurConsole(ROUGE);
			
			fprintf(stderr, "ERREUR : Caract%sre de contr%sle d%stect%s dans le fichier source :\n", eAccGrave, oAccCirconflexe, eAccAigu, eAccAigu);
			fprintf(stderr, "[SUB] position ASCII : 26\n");
			fprintf(stderr, "N'utilisez aucun caract%sre de contr%sle dans les fichiers source.\n", eAccGrave, oAccCirconflexe);
			
			definirCouleurConsole(NORMAL);
			
			exit(1);
		}
		
		int indRecherche = 0;
		
		if(estDedans(eth->tabCorrespondance, c)) // Vérifie si un caractère se situe dans la table de correspondance caractère-code de l'en-tête
			indRecherche = recherche(eth->tabCorrespondance, c); // Donne l'indice du code d'un caractère
		
		#if defined(POSIX)
			uchar* tmp = malloc((sizeof(uchar) * strlen((char*)chaine)));
			strcpy((char*)tmp, (char*)chaine);
			free(chaine);
			chaine = malloc((sizeof(uchar) * (strlen((char*)tmp) + strlen((char*)eth->tabCorrespondance[indRecherche].code))));
			strcpy((char*)chaine, (char*)tmp);
			free(tmp);
		#endif
		
		strcat(((char*)chaine), ((char*)eth->tabCorrespondance[indRecherche].code)); // --- Jusqu'ici, remplissage de la chaîne temporaire. ---
		
		if(strlen(((char*)chaine)) >= 7) // --- Ici, début du traitement de l'octet. ---
		{
			while(strlen(((char*)chaine)) >= 7)
			{
				uchar octet = 0;
				int nbBits = 0;
				
				octet = octet | 1;
				octet = octet << 1;
				
				for(int i=0;i<8;++i)
				{
					int bit = (chaine[nbBits] - '0');
					octet = octet | bit;
					++nbBits;
					
					if(nbBits == 7)
					{
						fputc(octet, fichierDestination);
						
						supprimerCaracteres(chaine, nbBits);
						cmptBitsCompresses += nbBits;
						
						nbBits = 0;
					}
					else
					{
						octet = octet << 1;
					}
				}
			}
		}
	}
	
	#if defined(WINDOWS)
		fseek(fichierSource, 0, SEEK_SET);
		
		if(fichierSource == NULL)
			fclose(fichierSource);
	#endif
	
	#if defined(POSIX)
		uchar* tmp = malloc((sizeof(uchar) * strlen((char*)chaine)));
		strcpy((char*)tmp, (char*)chaine);
		free(chaine);
		chaine = malloc((sizeof(uchar) * (strlen((char*)tmp) + 8)));
		strcpy((char*)chaine, (char*)tmp);
		free(tmp);
	#endif
	
	strcat(((char*)chaine), "00000000"); // Remplissage du dernier octet
	
	while(strlen(((char*)chaine)) >= 7) // Traitement du dernier octet
	{
		uchar octet = 0;
		int nbBits = 0;
		
		octet = octet | 1;
		octet = octet << 1;
		
		for(int i=0;i<8;++i)
		{
			int bit = (chaine[nbBits] - '0');
			octet = octet | bit;
			++nbBits;
			
			if(nbBits == 7)
			{
				fputc(octet, fichierDestination);
				
				supprimerCaracteres(chaine, nbBits);
				cmptBitsCompresses += nbBits;
				
				nbBits = 0;
			}
			else
			{
				octet = octet << 1;
			}
		}
	}
	
	#if defined(WINDOWS)
		fseek(fichierDestination, 0, SEEK_SET);
		
		if(fichierDestination == NULL)
			fclose(fichierDestination);
	#endif
	
	chiffresHuffman(a, eth, cmptBitsCompresses);
}

// --- Décompression ---

void chaineCompresseeVersChaineBinaire(uchar* chaineCompressee)
{
	for(size_t i=0;i<strlen((char*)chaineCompressee);++i)
	{
		affichageBinaireDUnCaractere(chaineCompressee[i]);
	}
	
	printf("\n");
	printf("\n\n");
}

void decompressionHuffman(uchar* chaineCompressee, Arbre* a, char* nomFichierDecompresse)
{
	FILE* fichierDecompresse = fopen(nomFichierDecompresse, "w");
	
	if(fichierDecompresse == NULL)
	{
		definirCouleurConsole(ROUGE);
		
		fprintf(stderr, "ERREUR : Impossible d'ouvrir le fichier \"%s\" en mode %scriture.\n", nomFichierDecompresse, eAccAigu);
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	uchar* binaire = caractereVersBinaire(chaineCompressee[0]);
	supprimerUnCaractere(binaire, 0); // On supprime le premier bit du premier octet car il s'agit d'un bit "barrière".
	supprimerUnCaractere(chaineCompressee, 0); // On supprime le premier octet car il est déjà lu.
	
	int cmptCar = 0; // On définit un compteur de caractères pour éviter de tomber sur le début d'un octet.
	int racine = 0; // On crée la variable destinée à contenir l'indice de la racine de l'arbre.
	
	while(a->tabNoeuds[racine].parent != -1) // Recherche de la racine de l'arbre
	{
		++racine;
	}
	
	definirCouleurConsole(JAUNE);
	printf("\n===================================================\n");
	definirCouleurConsole(MAGENTA);
	printf("Fichier d%scompress%s :\n", eAccAigu, eAccAigu);
	definirCouleurConsole(NORMAL);
	
	if(((a->taille + 1) / 2) == 1) // Si l'arbre ne contient qu'une seule feuille, alors...
	{
		for(int i=0;i<a->tabNoeuds[0].nombreOccurences;++i) // Pour chaque nombre d'occurrences du seul caractère de l'arbre...
		{
			fputc(a->tabNoeuds[0].c, fichierDecompresse); // On écrit le caractère de la feuille de l'arbre dans le fichier décompressé.
			printf("%c", a->tabNoeuds[0].c); // On écrit le caractère de la feuille de l'arbre dans la sortie standard.
		}
	}
	else // Sinon...
	{
		while(strlen(((char*)chaineCompressee)) > 0)
		{
			int positionCouranteArbre = racine; // On définit la position courante dans l'arbre.
			
			//if(!estFeuille(a, positionCouranteArbre)) // Si on n'est pas tombé sur une feuille, alors...
			while(!estFeuille(a, positionCouranteArbre)) // Si on n'est pas tombé sur une feuille, alors...
			{
				if(cmptCar < 7) // Si nous ne sommes toujours pas tombé sur le début d'un octet, alors...
				{
					if(binaire[0] == '0') // Si le caractère courant de la chaîne compressée est égale à 0, alors on va à gauche.
						positionCouranteArbre = a->tabNoeuds[positionCouranteArbre].gauche;
					else // Sinon, on va à droite.
						positionCouranteArbre = a->tabNoeuds[positionCouranteArbre].droit;
					
					++cmptCar;
					supprimerUnCaractere(binaire, 0); // On supprime le caractère courant car il est déjà lu.
				}
				else // Si nous sommes tombé sur le début d'un octet, alors...
				{
					binaire = caractereVersBinaire(chaineCompressee[0]);
					supprimerUnCaractere(binaire, 0); // On supprime le premier bit du premier octet car il s'agit d'un bit "barrière".
					supprimerUnCaractere(chaineCompressee, 0); // On supprime le premier octet car il est déjà lu.
					cmptCar = 0; // On remet le compteur de caractères à 0 pour parcourir l'octet suivant.
				}
			} // à la fin de cette boucle, nous sommes tombés sur une des feuilles de l'arbre.
			
			fputc(a->tabNoeuds[positionCouranteArbre].c, fichierDecompresse); // On écrit le caractère de la feuille de l'arbre dans le fichier décompressé.
			printf("%c", a->tabNoeuds[positionCouranteArbre].c); // On écrit le caractère de la feuille de l'arbre dans la sortie standard.
		}
	}
	
	fclose(fichierDecompresse);
	
	definirCouleurConsole(JAUNE);
	printf("\n===================================================\n");
	definirCouleurConsole(NORMAL);
}
#ifndef INCLUSIONS_H
#define INCLUSIONS_H

/*! \file Inclusions.h
 * \brief Contient le nécessaire pour l'inclusion de fichiers d'en-têtes.
 * \details Ce fichier contient le, voire les fichier(s), à inclure afin de veiller au bon déroulement de ce projet.
 * 			Il contient également la documentation de la première page de la documentation générée par la doxygen,
 * 			la page "index.html".
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "Calculs.h"
#include "Noel.h"
#include "Huffman.h"

/*! \mainpage Index de la documentation.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 * \section intro_sec Introduction
 * 					  Ce projet a pour but de réaliser une implémentation de la Compression de Huffman. Le choix des
 * 					  paradigmes utilisés, bien qu'en apparence limité, était libre.
 * \section sec1 Description du projet
 * 					  Nous devions réaliser, à la base, deux programmes en C bien distincts, ainsi qu'un programme en
 * 					  Python. L'un des programmes en C, devant s'appeler "huff.c", devait pouvoir compresser un fichier
 * 					  via la méthode de Huffman. L'autre programme en langage C devait, quant à lui, s'appeler "dehuff.c",
 * 					  et pouvoir décompresser un fichier. Mais, suite à une conception de projet particulière, nous nous
 * 					  sommes rendus compte en fin de projet que trop de paramètres seraient à modifier dans l'organisation
 * 					  du projet et sa compilation pour le faire en deux programme distincts. \n
 * 					  Le projet est donc utilisable uniquement via des options en ligne de commande dans un seul et unique
 * 					  exécutable. \n
 * 					  La partie en Python, quant à elle, permet de créer une archive d'un répertoire avec l'exécutable produit
 * 					  avec le code source en C. Il est également possible de décompresser l'archive via le programme écrit
 * 					  en Python.
 * \section sec2 Composition du code source
 * 				Le code source du projet est légèrement différent entre le C et le Python. \n
 * 				Le code source se compose de la manière suivante :
 * 					- un fichier regroupant des fonctions de manipulation de fichiers : \ref page1 ;
 * 					- un répertoire regroupant plusieurs fichier de macros spécifiques aux systèmes d'exploitation et aux langages :
 * 						- un fichier incluant tous les fichiers de macros : \ref page2 ;
 * 						- un fichier définissant les macros liées aux systèmes d'exploitation : \ref page3 ;
 * 						- un fichier définissant les macros liées aux langages : \ref page4 ;
 * 						- un fichier définissant les macros liées aux commandes spécifiques des systèmes d'exploitation : \ref page5 ;
 * 						- un fichier définissant les macros liées à l'écriture en couleur dans la console : \ref page6 ;
 * 						- un fichier définissant les macros liées à l'écriture des caractères spéciaux dans la console : \ref page7 ;
 * 					- un fichier pour la représentation d'un noeud de Huffman : \ref page8 ;
 * 					- un fichier pour la représentation et la manipulation d'un arbre de Huffman, les codes et les en-têtes : \ref page9 ;
 * 					- un fichier pour la compression et la décompression de Huffman : \ref page10 ;
 * 					- un fichier pour le calcul des probabilités des caractères : \ref page11 ;
 * 					- un fichier pour les fêtes de fin d'année 2020 : \ref page12.
 * \section sec3 Documentation du Python
 * 				La partie en Python est documentée de la même manière que le C, les pages associées comme cela a été fait pour
 * 				la partie en C n'a pas été faite car cela nous a paru superflu par rapport au peu de code rédigé et aux explications
 * 				déjà présentes qui nous semblaient suffisamment claires.
 */

#endif // INCLUSIONS_H
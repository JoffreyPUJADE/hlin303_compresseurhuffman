ifeq ($(OS),Windows_NT)
	Linux=no
	UNAME_S = Windows_NT
else
	UNAME_S=$(shell uname -s)
endif

ifeq ($(UNAME_S),Linux)
	Linux=yes
endif

Debug=yes
Opt=yes
OptLvl=3

ifeq ($(Debug),yes)
	DebugFlag=-g
else
	DebugFlag=
endif

ifeq ($(Opt),yes)
	ifeq ($(OptLvl),0)
		OptimisationFlag=-O
	else ifeq ($(OptLvl),1)
		OptimisationFlag=-O
	else ifeq ($(OptLvl),2)
		OptimisationFlag=-O2
	else ifeq ($(OptLvl),3)
		OptimisationFlag=-O3
	endif
else
	OptimisationFlag=
endif

DirSrc=src
DirObj=obj
DirLib=lib

MainSourceFile=main
HeaderNeeded=include/Inclusions.h

Compilo=gcc
Liens=-o
Preproc=-c $(DebugFlag) -Wall

ifeq ($(Linux),yes)
	DrapeauxPreproc=-lm
	DrapeauxLiens=$(DrapeauxPreproc)
	#RM=rm -f
	RM=rm -rf
	Slash=/
	Extension=
else
	DrapeauxPreproc=-lkernel32
	DrapeauxLiens=$(DrapeauxPreproc)
	RM=del
	Slash=\\
	Extension=.exe
endif

Src=$(wildcard $(DirSrc)/*.c)
Obj=$(patsubst $(DirSrc)/%.c,$(DirObj)/%.o,$(Src))
Exec=$(MainSourceFile)$(Extension)

all: $(Exec)

$(Exec): $(Obj)
	$(Compilo) $(Liens) $@ $^ $(DrapeauxLiens)

$(DirObj)/$(MainSourceFile).o: $(HeaderNeeded) | $(DirObj)

$(DirObj)/%.o: $(DirSrc)/%.c | $(DirObj)
	$(Compilo) $(Liens) $@ $< $(Preproc) $(DrapeauxPreproc)

$(DirObj):
	mkdir $@

.PHONY: clean mrproper

clean:
	$(RM) $(DirObj)$(Slash)*.o
	$(RM) $(DirObj)

mrproper:
	$(RM) $(Exec)

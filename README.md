# HLIN303_CompresseurHuffman
Développement du Compresseur de Huffman en C dans le cadre du projet de contrôle continu de l’UE “Systèmes d’exploitation” (HLIN303).

# Sommaire

* [Compilation](#compilation)
* [Utilisation](#utilisation)
	* [Partie en C](#partie-en-c)
		* [Aide en C](#aide-en-c)
		* [Utilisation du C](#utilisation-du-c)
	* [Partie en Python](#partie-en-python)
		* [Aide en Python](#aide-en-Python)
		* [Utilisation du Python](#utilisation-du-python)

# Documentation

Une documentation a été écrite via Doxygen ; un fichier de configuration `Doxyfile` est présent dans la racine du répertoire du projet.
Afin de générer cette dernière, il faut ouvrir un terminal dans la racine du projet et taper la commande `doxygen` dans le terminal.
La documentation se génèrera, sous format `HTML`, dans le répertoire `doc`. Il suffira alors d'ouvrir le fichier `index.html`, contenu
dans le répertoire `doc/html/`.

ATTENTION : Il est préférable d'avoir les outils sous Windows ou sous Linux permettant la création de graphiques afin d'avoir une meilleure
documentation générée.

# Compilation

La compilation, que ce soit sous Linux ou sous Windows, se fait via le Makefile, avec la commande `make`.

# Utilisation

## Partie en C

Une aide à l'utilisation est disponible directement dans l'exécutable. Plus bas sera également expliqué l'utilisation du programme
en C.

### Aide en C

Il suffit de taper (sous Windows) la commande `main.exe -h` ou `main.exe --help`.

Il suffit de taper (sous Linux) la commande `./main -h` ou `./main --help`.

ATTENTION : Sous Linux, avant de tenter d'exécuter le programme, il faut tout d'abord le RENDRE EXÉCUTABLE après la compilation, avec la commande `chmod u+x main ./main`.

### Utilisation du C

L'option `-c` ou `--compresser` permet la compression d'un fichier via la méthode de Huffman ; deux arguments sont nécessaires pour
cette option : le premier argument est le fichier source, donc le fichier à compresser, et le second argument est le fichier de destination,
donc le fichier compressé.

L'option `d` ou `--decompresser` permet la décompression d'un fichier via la méthode de Huffman ; un seul argument est nécessaire pour
cette option : le fichier compressé.

L'option `-n` ou `--noel` est une option surprise à l'occasion des fêtes de fin d'année 2020. Elle ne nécessite aucun argument.

## Partie en Python

Une aide à l'utilisation est également disponible dans l'exécutable Python. Plus bas sera, comme pour le C, expliqué l'utilisation du
programme en Python.

### Aide en Python
 
L'aide à l'utilisation est dans l'exécutable `main.py`, avec les arguments `-h` ou `--help` également.

### Utilisation du Python

L'option `-c` ou `--compresser` permet la compression d'un répertoire via la méthode de Huffman ; deux arguments sont nécessaires pour
cette option : le premier argument est le répertoire source, donc le répertoire à compresser, et le second argument est l'archive à créer.

L'option `d` ou `--decompresser` permet la décompression d'une archive via la méthode de Huffman ; un seul argument est nécessaire pour
cette option : l'archive à décompresser.

#ifndef HUFFMAN_H
#define HUFFMAN_H

/*! \page page10 Huffman
 * \par Description du fichier \a "Huffman.h" \a 
 * 		Le fichier \a "Huffman.h" \a contient le nécessaire pour réaliser la compression a décompression de Huffman.
 * 		
 * 		Il contient les fonctions suivantes :
 * 			- ::obtenirLeIemeBit(uchar c, int i) ;
 * 			- ::caractereVersBinaire(uchar ch) ;
 * 			- ::affichageBinaireDUnCaractere(uchar ch) ;
 * 			- ::chiffresHuffman(char* nomFichier, Arbre* a, EnTeteHuffman const* eth, int nbBitsCompresses) ;
 * 			- ::compressionHuffman(char* nomFichierSource, char* nomFichierDestination, Arbre* a, EnTeteHuffman const* eth) ;
 * 			- ::chaineCompresseeVersChaineBinaire(uchar* chaineCompressee) ;
 * 			- ::decompressionHuffman(uchar* chaineCompressee, Arbre* a, char* nomFichierDecompresse).
 */

/*! \file Huffman.h
 * \brief Contient tout le nécessaire pour compresser/décompresser.
 * \details Ce fichier contient le nécessaire pour réaliser la compression a décompression de Huffman.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "Arbre.h"
#include "Calculs.h"
#include <stdlib.h>

// --- Manipulation du binaire ---

/*! \fn uchar obtenirLeIemeBit(uchar c, int i)
 * \brief Renvoie un bit d'un caractère.
 * \param [in] c (uchar) : Caractère dont il faut récupérer un bit.
 * \param [in] i (int) : Position du bit à récupérer.
 * \details Cette fonction permet d'obtenir un bit de la position décimale du caractère dans la
 * 			table ASCII.
 * \return Bit du caractère (uchar).
 * \callergraph
 * \callgraph
 */
uchar obtenirLeIemeBit(uchar c, int i);

/*! \fn uchar* caractereVersBinaire(uchar ch)
 * \brief Convertit un caractère en binaire.
 * \param [in] ch (uchar) : Caractère à convertir.
 * \details Cette fonction permet de convertir la position décimale d'un caractère dans la table
 * 			ASCII en binaire codé sur 8 bits (soit un octet).
 * \return Caractère convertit en binaire (uchar*).
 * \callergraph
 * \callgraph
 */
uchar* caractereVersBinaire(uchar ch);

/*! \fn void affichageBinaireDUnCaractere(uchar ch)
 * \brief Affiche un caractère en binaire.
 * \param [in] ch (uchar) : Caractère à afficher en binaire.
 * \details Cette procédure permet de convertir la position décimale d'un caractère dans la table
 * 			ASCII en binaire codé sur 8 bits (soit un octet) et de l'afficher.
 * \callergraph
 * \callgraph
 */
void affichageBinaireDUnCaractere(uchar ch);

// --- Compression ---

/*! \fn void chiffresHuffman(char* nomFichier, Arbre* a, EnTeteHuffman* eth, uchar* fichierCompresse)
 * \brief Affiche les résultats de la compression de Huffman.
 * \param [in] nomFichier (char*const) : Nom du fichier source.
 * \param [inout] a (Arbre*) : Arbre de Huffman à afficher.
 * \param [in] eth (EnTeteHuffman const*) : En-tête de Huffman à afficher.
 * \param [inout] fichierCompresse (uchar*) : Contenu du fichier compressé.
 * \details Cette procédure permet d'afficher les résultats de la compression d'un fichier avec la
 * 			méthode de Huffman.
 * \callergraph
 * \callgraph
 */
void chiffresHuffman(Arbre* a, EnTeteHuffman const* eth, int nbBitsCompresses);

/*! \fn void compressionHuffman(char* nomFichierSource, char* nomFichierDestination, Arbre* a, EnTeteHuffman* eth)
 * \brief Compresse un fichier.
 * \param [in] nomFichierSource (char const*) : Nom du fichier source.
 * \param [in] nomFichierDestination (char const*) : Nom du fichier de destination.
 * \param [inout] a (Arbre*) : Arbre de Huffman.
 * \param [in] eth (EnTeteHuffman const*) : En-tête de Huffman à écrire dans le fichier de destination.
 * \details Cette procédure permet de compresser un fichier avec la méthode de Huffman.
 * \callergraph
 * \callgraph
 */
void compressionHuffman(char const* nomFichierSource, char const* nomFichierDestination, Arbre* a, EnTeteHuffman const* eth);

// --- Décompression ---

/*! \fn void chaineCompresseeVersChaineBinaire(uchar* chaineCompressee)
 * \brief Affiche une chaîne binaire.
 * \param [inout] chaineCompressee (uchar*) : Chaîne compressée à afficher en binaire.
 * \details Cette procédure permet d'afficher une chaîne compressée au format ASCII en binaire.
 * \callergraph
 * \callgraph
 */
void chaineCompresseeVersChaineBinaire(uchar* chaineCompressee);

/*! \fn void decompressionHuffman(uchar* chaineCompressee, Arbre* a, char* nomFichierDecompresse)
 * \brief Décompresse un fichier.
 * \param [inout] chaineCompressee (uchar*) : Chaîne compressée à décompresser.
 * \param [inout] a (Arbre*) : Arbre de Huffman reconstruit.
 * \param [inout] nomFichierDecompresse (char*) : Nom original du fichier source (nom du fichier décompressé).
 * \details Cette procédure permet de décompresser un fichier avec la méthode de Huffman.
 * \callergraph
 * \callgraph
 */
void decompressionHuffman(uchar* chaineCompressee, Arbre* a, char* nomFichierDecompresse);

#endif // HUFFMAN_H
#include "../include/Calculs.h"

void calculProba(EnTeteHuffman const* eth)
{
	for(int i=0;i<256;++i)
	{
		if(estDedans(eth->tabCorrespondance, i))
			printf("car : %c de code : %x de proba : %f\n", i, i, eth->tabCorrespondance[recherche(eth->tabCorrespondance, i)].nombreOccurences/(float)eth->tailleOriginaleFichier);
	}
}
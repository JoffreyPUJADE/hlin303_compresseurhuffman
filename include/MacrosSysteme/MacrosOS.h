#ifndef MACROSOS_H
#define MACROSOS_H

/*! \page page3 MacrosOS
 * \par Description du fichier \a "MacrosOS.h" \a 
 * 		Le fichier \a "MacrosOS.h" \a définit différentes macros pour détecter le système d'exploitation sous lequel
 * 		le programme est compilé.
 * 		
 * 		Les macros définies sont :
 * 			- WINDOWS : Pour le système d'exploitation Windows ;
 * 			- POSIX : Pour les systèmes d'exploitation Posix (comme Linux, par exemple) ;
 * 			- MACOS : Pour le système d'exploitation MacOS.
 * 		
 * 		Une autre macro est également définie dans ce fichier : FUNCTION_NAME, pour obtenir le nom de la fonction courante.
 */

/*! \file MacrosOS.h
 * \brief Contient tout le nécessaire pour détecter le système d'exploitation.
 * \details Ce fichier définit différentes macros pour détecter le système d'exploitation sous lequel le programme est
 * 			compilé. Les macros définies sont :
 * 				- WINDOWS : Pour le système d'exploitation Windows ;
 * 				- POSIX : Pour les systèmes d'exploitation Posix (comme Linux, par exemple) ;
 * 				- MACOS : Pour le système d'exploitation MacOS.
 * 			Une autre macro est également définie dans ce fichier : FUNCTION_NAME, pour obtenir le nom de la fonction courante.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

// Définition des macros pour les systèmes d'exploitation.
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(WINDOWS) || defined(__WINDOWS__) || defined(_Windows) || defined(_WIN64) || defined(_WIN16) || defined(__TOS_WIN__)
	#ifndef WINDOWS
		#define WINDOWS /*!< Cette macro est définie par le compilateur si le programme est compilé sous Windows. */
	#endif // WINDOWS
#elif defined(unix) || defined(UNIX) || defined(__UNIX__) || defined(POSIX) || defined(__POSIX__) || defined(CYGWIN) || defined(linux) || defined(__linux__) || defined(__linux)
	#ifndef POSIX
		#define POSIX /*! Cette macro est définie par le compilateur si le programme est compilé sous un système Posix. */
	#endif // POSIX
#elif defined(__APPLE__) || defined(__MACH__) || defined(Macintosh) || defined(macintosh)
	#ifndef MACOS
		#define MACOS /*! Cette macro est définie par le compilateur si le programme est compilé sous un système Mac OS. */
	#endif // MACOS
#endif // WIN32 _WIN32 __WIN32__ WINDOWS __WINDOWS__ _Windows _WIN64 _WIN16 __TOS_WIN__ unix UNIX __UNIX__ POSIX __POSIX__ CYGWIN linux __linux__ __linux __APPLE__ __MACH__ Macintosh macintosh

// Définition d'une macro pour connaître la fonction courante selon les systèmes d'exploitation et les compilateurs.
#ifndef FUNCTION_NAME
	#ifdef WINDOWS
		#ifdef _MSC_VER
			#define FUNCTION_NAME __FUNCTION__
		#else
			#define FUNCTION_NAME __func__
		#endif
	#else
		#if defined(POSIX) || defined(MACOS)
			#define FUNCTION_NAME __func__
		#endif
	#endif
#endif // FUNCTION_NAME

#endif // MACROSOS_H
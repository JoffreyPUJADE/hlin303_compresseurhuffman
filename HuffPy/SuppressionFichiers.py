#coding: UTF-8

## @package SuppressionFichiers
# @brief Ce fichier regroupe les fonctions de suppressions de fichiers.
# @details La compression et la décompression des archives nécessitent l'emploi de fichiers intermédiaires ; par conséquent, ces fonctions
# 		   sont là pour permettre la suppression de ceux-ci lorsque la compression et la décompression des archives sont terminées.
# @authors Joffrey PUJADE, Tom TISSERANT
# @date 10-12-2020
# @version 0.1

import os
import HuffPy.HuffExcept as he

## @fn supprimerFichiersCompression(listeFic)
# @brief Supprime des fichiers compressés.
# @param [in] listeFic (list) : Liste contenant les chemins et noms des fichiers source ainsi que les noms des fichiers de destination.
# @details Cette procédure permet de supprimer tous les fichiers compressés avec la méthode de Huffman du répertoire courant.
# @callergraph
# @callgraph
def supprimerFichiersCompression(listeFic) :
	for fichier in listeFic :
		if os.path.exists(fichier[1]) :
			os.remove(fichier[1])
		else :
			raise he.HuffExcept("ERREUR : Le fichier \"" + fichier[1] + "\" est inexistant.")

##@fn supprimerInfosArchive()
# @brief Supprime le fichier d'infos.
# @details Cette procédure permet de supprimer le fichier "infosArchive", contenant les sous-répertoire du répertoire d'origine,
#		   du répertoire courant.
# @callergraph
# @callgraph
def supprimerInfosArchive() :
	if os.path.exists("infosArchive") :
		os.remove("infosArchive")
	else :
		raise he.HuffExcept("ERREUR : Le fichier \"infosArchive\" est inexistant.")

## @fn supprimerIntermediairesCompression(listeFic)
# @brief Supprime les fichiers intermédiaires.
# @param [in] listeFic (list) : Liste contenant les chemins et noms des fichiers source ainsi que les noms des fichiers de destination.
# @details Cette procédure permet de supprimer tous les fichiers "intermédiaires" de compression du fichier courant, c'est-à-dire
# 		   tous ceux qui on été générés lors de la création de l'archive (i.e. les fichiers compressés du répertoire courant et le
# 		   fichier "infosArchive").
# @callergraph
# @callgraph
def supprimerIntermediairesCompression(listeFic) :
	supprimerFichiersCompression(listeFic)
	supprimerInfosArchive()

## @fn supprimerArchive(nomArchive)
# @brief Supprime l'archive.
# @param [in] nomArchive : Nom de l'archive à supprimer.
# @details Cette procédure supprime l'archive créée lors de la compression.
# @callergraph
# @callgraph
def supprimerArchive(nomArchive) :
	if os.path.exists(nomArchive) :
		os.remove(nomArchive)
	else :
		raise he.HuffExcept("ERREUR : Le fichier \"" + nomArchive + "\" est inexistant.")

## @fn supprimerFichiersHuffman(listeFicHuff)
# @brief Supprime des fichiers compressés.
# @param [in] listeFicHuff (list) : Liste contenant les chemins et noms des fichiers source ainsi que les noms des fichiers de destination.
# @details Cette procédure permet de supprimer tous les fichiers compressés avec la méthode de Huffman du répertoire courant.
# @callergraph
# @callgraph
def supprimerFichiersHuffman(listeFicHuff) :
	for fichierHuff in listeFicHuff :
		if os.path.exists(fichierHuff) :
			os.remove(fichierHuff)
		else :
			raise he.HuffExcept("ERREUR : Le fichier \"" + fichierHuff + "\" est inexistant.")

## @fn supprimerIntermediairesDecompression(nomArchive, listeFicHuff)
# @brief Supprime les fichiers intermédiaires.
# @param [in] nomArchive : Nom de l'archive à supprimer.
# @param [in] listeFicHuff (list) : Liste contenant les chemins et noms des fichiers source ainsi que les noms des fichiers de destination.
# @details Cette procédure permet de supprimer tous les fichiers "intermédiaires" de décompression du fichier courant, c'est-à-dire
# 		   tous ceux qui on été générés lors de la décompression de l'archive (i.e. les fichiers compressés du répertoire courant et le
# 		   fichier "infosArchive").
# @callergraph
# @callgraph
def supprimerIntermediairesDecompression(nomArchive, listeFicHuff) :
	supprimerArchive(nomArchive)
	supprimerInfosArchive()
	supprimerFichiersHuffman(listeFicHuff)
#ifndef MACROSLANGAGE_H
#define MACROSLANGAGE_H

/*! \page page4 MacrosLangage
 * \par Description du fichier \a "MacrosLangage.h" \a 
 * 		Le fichier \a "MacrosLangage.h" \a définit différentes macros pour détecter le langage de programmation
 * 		utilisé (le C ou le C++).
 * 		
 * 		Voici les tableaux d'explications des macros définies dans ce fichier :
 * 		-------------------------------------------------------
 * 		| Nom des macros | Version des langages               |
 * 		|----------------|------------------------------------|
 * 		| C89_C90        | C89 et C90                         |
 * 		| C94            | C94                                |
 * 		| C99            | C99                                |
 * 		| C11            | C11                                |
 * 		| C18            | C18                                |
 * 		| CPP98          | C++98                              |
 * 		| CPP11          | C++11                              |
 * 		| CPP14          | C++14                              |
 * 		| CPP17          | C++17                              |
 * 		| CPP_CLI        | C++/CLI                            |
 * 		| ECPP           | EC++ (C++ embarqué / Embedded C++) |
 * 		-------------------------------------------------------
 * 		
 * 		----------------------------------------------------------------------------------------
 * 		| Nom des macros     | Signification                                                   |
 * 		|--------------------|-----------------------------------------------------------------|
 * 		| C_LANGUAGE         | Une des versions du langage C.                                  |
 * 		| CPP_LANGUAGE       | Une des versions du langage C++ (hors CPP_CLI et ECPP).         |
 * 		| OTHER_CPP_LANGUAGE | Soit CPP_CLI, soit ECPP, soit les deux.                         |
 * 		| ALL_CPP_LANGUAGES  | Une des versions du langage C++ parmi toutes celles existantes. |
 * 		----------------------------------------------------------------------------------------
 */

/*! \file MacrosLangage.h
 * \brief Contient tout le nécessaire pour détecter le langage de programmation.
 * \details Ce fichier définit différentes macros pour détecter le langage de programmation utilisé (le C ou le C++).
 * 			Toutes les macros que définit ce fichier sont listées et expliquées dans deux tableaux qui sont contenus
 * 			dans la page \ref page4. Les normes du C qui sont détectables via ces macros vont du C89/90 au C18 et les
 * 			normes du C++ qui sont détectables via ces macros vont du C++98 au C++17, passant aussi par le C++/CLI et
 * 			le C++ embarqué (Embedded C++).
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#if defined(__STDC__)
	#ifndef C89_C90
		#define C89_C90
	#endif // C89_C90
#elif defined(__STDC_VERSION__)
	#if (__STDC_VERSION__ == 199409L)
		#ifndef C94
			#define C94
		#endif // C94
	#elif (__STDC_VERSION__ == 199901L)
		#ifndef C99
			#define C99
		#endif // C99
	#elif (__STDC_VERSION__ == 201112L)
		#ifndef C11
			#define C11
		#endif // C11
	#elif (__STDC_VERSION__ == 201710L)
		#ifndef C18
			#define C18
		#endif // C18
	#endif
#elif defined(__cplusplus)
	#if (__cplusplus == 199711L)
		#ifndef CPP98
			#define CPP98
		#endif // CPP98
	#elif (__cplusplus == 201103L)
		#ifndef CPP11
			#define CPP11
		#endif // CPP11
	#elif (__cplusplus == 201402L)
		#ifndef CPP14
			#define CPP14
		#endif // CPP14
	#elif (__cplusplus == 201703L)
		#ifndef CPP17
			#define CPP17
		#endif // CPP17
	#endif
#elif defined(__cplusplus_cli)
	#if (__cplusplus_cli == 200406L)
		#ifndef CPP_CLI
			#define CPP_CLI
		#endif // CPP_CLI
	#endif
#elif defined(__embedded_cplusplus)
	#ifndef ECPP
		#define ECPP
	#endif // ECPP
#endif

#if defined(C89_90) || defined(C94) || defined(C99) || defined(C11) || defined(C18)
	#ifndef C_LANGUAGE
		#define C_LANGUAGE
	#endif // C_LANGUAGE
#elif defined(CPP98) || defined(CPP11) || defined(CPP14) || defined(CPP17)
	#ifndef CPP_LANGUAGE
		#define CPP_LANGUAGE
	#endif // CPP_LANGUAGE
#elif defined(CPP_CLI) || defined(ECPP)
	#ifndef OTHER_CPP_LANGUAGE
		#define OTHER_CPP_LANGUAGE
	#endif // OTHER_CPP_LANGUAGE
#endif

#if defined(CPP_LANGUAGE) || defined(OTHER_CPP_LANGUAGE)
	#ifndef ALL_CPP_LANGUAGES
		#define ALL_CPP_LANGUAGES
	#endif // ALL_CPP_LANGUAGES
#endif

#endif // MACROSLANGAGE_H
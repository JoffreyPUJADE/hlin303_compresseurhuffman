#coding: UTF-8

## @package HuffExcept
# @brief Contient le nécessaire pour lancer un type d'erreur personnalisé.
# @details Ce fichier contient la classe "HuffExcept", qui est un type d'erreur "personnalisé". Elle est héritée de la classe
# 		   "Exception". Elle permet juste l'utilisation des exception dans un contexte plus "approprié" que pour les exceptions
# 			prédéfinies dans le langage Python.
# @authors Joffrey PUJADE, Tom TISSERANT
# @date 12-12-2020
# @version 0.1

import HuffPy.Courant as cour

## @fn infosCourantes()
# @brief Donne des informations courantes.
# @details Cette fonction renvoie une liste contenant les informations courantes de l'appel de la fonction.
# 		   Ces informations courantes sont :
# 		   - le nom du fichier courant (index 0) ;
# 		   - la ligne courante (index 1) ;
# 		   - le nom de la fonction dans laquelle a lieu l'appel (index 2) ;
# 		   - la trace d'exécution de l'appel (index 3).
# @return Liste d'informations courantes (list).
# @callergraph
# @callgraph
def infosCourantes() :
	return [cour.btFichier(3), cour.btLigne(3), cour.btNom(3), cour.btBackTrace(3)]

## @brief Type d'erreur "personnalisé".
# @details Cette classe est un type d'erreur à l'exécution permettant de connaître, en plus du message d'erreur standard, le
# 		   fichier, la fonction et la ligne où l'erreur a été lancée. Aussi, il est également possible d'avoir la trace
# 		   d'exécution de l'erreur si demandé.
class HuffExcept(Exception) :
	## @brief Constructeur de la classe.
	# @param [in] self (HuffExcept) : Instance d'une erreur.
	# @param [in] erreur (string) : Message d'erreur de l'instance.
	# @param [in] infos (list) : Liste des informations de la fonction créant l'instance (nom du fichier, ligne, fonction et trace d'exécution).
	# @param [in] backtrace (bool) : Active ou non la trace d'exécution.
	# @callergraph
	# @callgraph
	def __init__(self, erreur, infos = infosCourantes(), backtrace = True) :
		super(HuffExcept, self).__init__(erreur)
		
		## Cet attribut est le message d'erreur.
		self.m_erreur = erreur
		
		## Cet attribut est le nom du fichier de l'erreur.
		self.m_fichier = infos[0]
		
		## Cet attribut est la ligne de l'erreur.
		self.m_ligne = infos[1]
		
		## Cet attribut est le nom de la fonction de l'erreur.
		self.m_fonction = infos[2]
		
		## Cet attribut détermine si la trace d'exécution de l'erreur est affichée ou non.
		self.m_possedeBackTrace = backtrace
		
		## Cet attribut est la trace d'exécution de l'erreur.
		self.m_backtrace = infos[3]
		
		if not self.m_possedeBackTrace :
			slf.m_backtrace = "Aucune trace d'exécution disponible."
	
	## @brief Converti en chaîne.
	# @param [in] self (HuffExcept) : Instance d'une erreur.
	# @details Cette méthode est appelée automatiquement lors d'un lancement d'erreur ; elle converti l'instance
	# 		   courante en une chaîne de caractères.
	# @return Instance convertie (string).
	# @callergraph
	# @callgraph
	def __str__(self) :
		part1 = "ERRREUR LEVÉE DANS LA FONCTION \"" + self.m_fonction + "\" :\n"
		part2 = "Message de l'erreur : " + self.m_erreur + "\n"
		part3 = "Fonction : \"" + self.m_fonction + "\"\n"
		part4 = "Ligne : " + str(self.m_ligne) + "\n"
		part5 = "Fichier : \"" + self.m_fichier + "\"\n"
		part6 = "Trace d'exécution : "
		part7 = ""
		
		if self.m_possedeBackTrace :
			for bt in self.m_backtrace :
				tmp1 = "Fichier : " + bt[1]
				tmp2 = "ligne : " + str(bt[2])
				tmp3 = "fonction : " + bt[3]
				part7 += (tmp1 + " ; " + tmp2 + " ; " + tmp3 + "\n")
		else :
			part7 = self.m_backtrace
		
		return (part1 + part2 + part3 + part4 + part5 + part6 + part7)
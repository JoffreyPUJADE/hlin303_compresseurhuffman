#coding: UTF-8

## @package Courant
# @brief Contient le nécessaire pour obtenir des informations courantes.
# @details Ce fichier contient des informations sommaires sur des appels courants, tels que la ligne courante d'un
# 		   programme, le nom du fichier courant, le nom de la fonction en cours d'exécution ainsi que la trace
# 		   d'exécution du programme jusqu'à la ligne courante.
# @authors Joffrey PUJADE, Tom TISSERANT
# @date 12-12-2020
# @version 0.1

import inspect

## @fn obtenirContexte(numCont)
# @brief Donne le contexte voulu.
# @param [in] numCont (int) : Nombre de "remontées" de contexte.
# @details Cette fonction permet de donner un contexte d'exécution du programme à un "moment t" donné, en "remontant",
# 		   en quelques sortes, la pile d'exécution du programme.
# @return Contexte demandé (frame).
# @callergraph
# @callgraph
def obtenirContexte(numCont) :
	res = inspect.currentframe()
	
	for i in range(0, numCont) :
		res = res.f_back
	
	return res

## @fn cLigne()
# @brief Donne la ligne courante.
# @details Cette fonction donne la ligne courante du fichier courant du programme.
# @return Ligne courante (int).
# @callergraph
# @callgraph
def cLigne() :
	return inspect.currentframe().f_back.f_lineno

## @fn cFichier()
# @brief Donne le fichier courant.
# @details Cette fonction donne le nom du fichier courant du programme.
# @return Nom du fichier courant (string).
# @callergraph
# @callgraph
def cFichier() :
	return inspect.currentframe().f_back.f_code.co_filename

## @fn cNom()
# @brief Donne la fonction courante.
# @details Cette fonction donne le nom de la fonction courante du fichier courant du programme.
# @return Nom de la fonction courante (string).
# @callergraph
# @callgraph
def cNom() :
	return inspect.currentframe().f_back.f_code.co_name

## @fn cBackTrace()
# @brief Donne la trace d'exécution courante.
# @details Cette fonction donne la trace d'exécution courante du programme.
# @return Trace d'exécution courante (list) ou None s'il n'y en a pas (None).
# @callergraph
# @callgraph
def cBackTrace() :
	return inspect.getouterframes(inspect.currentframe())

## @fn contLigne(contexte)
# @brief Donne la ligne du contexte.
# @param [in] contexte (frame) : Contexte voulu.
# @details Cette fonction donne la ligne du contexte donné.
# @return Ligne du contexte (int).
# @callergraph
# @callgraph
def contLigne(contexte) :
	return icontexte.f_lineno

## @fn contFichier(contexte)
# @brief Donne le fichier du contexte.
# @param [in] contexte (frame) : Contexte voulu.
# @details Cette fonction donne le nom du fichier du contexte donné.
# @return Nom du fichier du contexte (string).
# @callergraph
# @callgraph
def contFichier(contexte) :
	return contexte.f_code.co_filename

## @fn contNom(contexte)
# @brief Donne la fonction du contexte.
# @param [in] contexte (frame) : Contexte voulu.
# @details Cette fonction donne le nom de la fonction du contexte donné.
# @return Nom de la fonction du contexte (string).
# @callergraph
# @callgraph
def contNom(contexte) :
	return contexte.f_code.co_name

## @fn contBackTrace(contexte)
# @brief Donne la trace d'exécution du contexte.
# @param [in] contexte (frame) : Contexte voulu.
# @details Cette fonction donne la trace d'exécution du contexte donné.
# @return Trace d'exécution du contexte (list) ou None s'il n'y en a pas (None).
# @callergraph
# @callgraph
def contBackTrace(contexte) :
	return inspect.getouterframes(contexte)

## @fn btLigne(nbBt)
# @brief Donne une ligne.
# @param [in] nbBt (int) : Nombre de "remontées" de contexte.
# @details Cette fonction donne une ligne selon le nombre de "remontées" dans la pile d'exécution donné.
# @return Ligne dans la pile d'exécution (int).
# @callergraph
# @callgraph
def btLigne(nbBt) :
	return obtenirContexte(nbBt).f_lineno

## @fn btFichier(nbBt)
# @brief Donne un fichier.
# @param [in] nbBt (int) : Nombre de "remontées" de contexte.
# @details Cette fonction donne un nom de fichier selon le nombre de "remontées" dans la pile d'exécution donné.
# @return Nom du fichier dans la pile d'exécution (string).
# @callergraph
# @callgraph
def btFichier(nbBt) :
	return obtenirContexte(nbBt).f_code.co_filename

## @fn btNom(nbBt)
# @brief Donne une fonction.
# @param [in] nbBt (int) : Nombre de "remontées" de contexte.
# @details Cette fonction donne le nom d'une fonction selon le nombre de "remontées" dans la pile d'exécution donné.
# @return Nom de la fonction dans la pile d'exécution (string).
# @callergraph
# @callgraph
def btNom(nbBt) :
	return obtenirContexte(nbBt).f_code.co_name

## @fn btBackTrace(nbBt)
# @brief Donne une trace d'exécution.
# @param [in] nbBt (int) : Nombre de "remontées" de contexte.
# @details Cette fonction donne une trace d'exécution selon le nombre de "remontées" dans la pile d'exécution donné.
# @return Trace d'exécution dans la pile d'exécution (list) ou None s'il n'y en a pas (None).
# @callergraph
# @callgraph
def btBackTrace(nbBt) :
	return inspect.getouterframes(obtenirContexte(nbBt))
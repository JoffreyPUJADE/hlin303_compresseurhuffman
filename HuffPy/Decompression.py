#coding: UTF-8

## @package Decompression
# @brief Ce fichier regroupe les fonctions de décompression d'archives.
# @details Les fonctions de ce fichier permettent la décompression d'archives créées avec les fonctions de compression et la méthode de Huffman.
# @authors Joffrey PUJADE, Tom TISSERANT
# @date 10-12-2020
# @version 0.1

import os
import zipfile as zip
import subprocess
import HuffPy.ParcoursFichiers as pf
import HuffPy.SuppressionFichiers as sf
import HuffPy.HuffExcept as he

## @fn decompressionArchive(nomArchive)
# @brief Décompresse une archive.
# @param [in] nomArchive (string) : Nom de l'archive à décompresser.
# @details Cette procédure permet de décompresser l'archive passée en paramètre afin de mettre les fichiers qui y sont contenus dans le répertoire courant.
# @callergraph
# @callgraph
def decompressionArchive(nomArchive) :
	with zip.ZipFile(nomArchive, 'r') as archive :
		archive.printdir()
		
		print("Extraction des fichiers de l'archive crée en cours...")
		
		archive.extractall()
		
		print("Extraction des fichiers de l'archive crée terminée.")

## @fn lireInfosArchive()
# @brief Lit les infos de l'archive.
# @details Cette fonction permet de lire le fichier "infosArchive", contenant les sous-répertoire du répertoire d'origine,
#		   du répertoire courant.
# @return Liste des sous-répertoires du répertoire à compresser (list).
# @callergraph
# @callgraph
def lireInfosArchive() :
	if not os.path.exists("infosArchive") :
		raise he.HuffExcept("ERREUR : Fichier \"infosArchive\" inexistant.", "lireInfosArchive")
	
	listeRepArchive = list()
	
	with open("infosArchive", 'r') as infosArchive :
		for ligne in infosArchive :
			listeRepArchive.append(ligne)
	
	for i in range(len(listeRepArchive)) :
		listeRepArchive[i] = listeRepArchive[i].rstrip("\n")
	
	return listeRepArchive

## @fn recreerRepertoireBase(listeRepArchive)
# @brief Recrée le répertoire.
# @param [in] listeRepArchive (list) : Liste des répertoires à recréer.
# @details Cette procédure permet de recréer le répertoire ainsi que les sous-répertoires à décompresser.
# @callergraph
# @callgraph
def recreerRepertoireBase(listeRepArchive) :
	for repertoire in listeRepArchive :
		if not os.path.exists(repertoire) :
			os.makedirs(repertoire)
		else :
			raise he.HuffExcept("ERREUR : Le répertoire \"" + repertoire + "\" existe déjà.")

## @fn decompressionDeHuffman(listeFicHuff)
# @brief Réalise la décompression de Huffman.
# @param [in] listeFicHuff (list) : Liste des fichiers compressés avec la méthode de Huffman à décompresser.
# @details Cette procédure permet de décompresser les fichiers compressés avec la méthode de Huffman.
# @callergraph
# @callgraph
def decompressionDeHuffman(listeFicHuff) :
	FNULL = open(os.devnull, 'w') # Aucun affichage sur la sortie standard (flux "stdout").
	
	for fichier in listeFicHuff :
		fichierCompresse = fichier
		
		arguments = "main -d" + fichierCompresse
		
		print("Début de la décompression de \"" + fichierCompresse + "\"...")
		
		subprocess.call(arguments, stdout = FNULL, stderr = FNULL, shell = False)
		
		print("Décompression de \"" + fichierCompresse + "\"  terminée.")

## @fn decompression(nomArchive)
# @brief Réalise la décompression complète.
# @param [in] nomArchive (string) : Nom de l'archive à décompresser.
# @details Cette procédure permet de décompresser entièrement une archive créée avec la compression et la méthode de Huffman
# 		   ainsi que de réaliser les traitements adéquats à la fin de la décompression de Huffman.
# @callergraph
# @callgraph
def decompression(nomArchive) :
	decompressionArchive(nomArchive)
	listeRepArchive = lireInfosArchive()
	recreerRepertoireBase(listeRepArchive)
	listeFicHuff = pf.listerFichiersHuffman(".")
	
	decompressionDeHuffman(listeFicHuff)
	
	sf.supprimerIntermediairesDecompression(nomArchive, listeFicHuff)
#include "../include/Arbre.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

Arbre *creerArbre(char* chemin)
{
	definirCouleurConsole(VERT);
	
	printf("D%sbut de la construction de l'arbre de Huffman.\n", eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	int nbOcc[256] = {0};
	int taille = 0;
	int nbFeuilles = 0;
	int c;
	FILE* f = fopen(chemin, "r"); // Ouverture du fichier "chemin" en lecture.
	
	if(f == NULL)
	{
		definirCouleurConsole(ROUGE);
		
		fprintf(stderr, "Impossible d'ouvrir le fichier \"%s\" en mode lecture.\n", chemin);
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	while((c = fgetc(f)) != EOF)
	{
		++taille;
		
		if(nbOcc[c] == 0)
		{
			++nbFeuilles;
		}
		
		++nbOcc[c];
	}
	
	fclose(f);
	
	Arbre* a = (Arbre*)malloc(sizeof(Arbre));
	
	a->taille = ((nbFeuilles * 2) - 1);
	a->tabNoeuds = (Noeud*)malloc(sizeof(Noeud) * a->taille);
	
	int indiceTab = 0; // Indice dans le tableau.
	
	for(int i=0;i<256;++i)
	{
		if(nbOcc[i] != 0)
		{
			a->tabNoeuds[indiceTab].c = i; // Caractère que l'on parcours.
			a->tabNoeuds[indiceTab].nombreOccurences = nbOcc[i]; // Nombre d'occurences.
			a->tabNoeuds[indiceTab].gauche = -1;
			a->tabNoeuds[indiceTab].droit = -1;
			a->tabNoeuds[indiceTab].parent = -1;
			++indiceTab;
		}
	}
	
	for(int nn=indiceTab;nn<a->taille;++nn)
	{
		// Choix des 2 plus petits noeuds orphelin (ceux qui ont le plus petit nombreOccurences).
		int min1 = taille;
		int min2 = taille;
		int imin1 = min1;
		int imin2 = min2;
		
		for(int i=0;i<nn;++i)
		{
			if((a->tabNoeuds[i].parent == -1) && (a->tabNoeuds[i].nombreOccurences < min1))
			{
				min2 = min1; // L'ancien plus petit devient le deuxième.
				imin2 = imin1;
				
				min1 = a->tabNoeuds[i].nombreOccurences;
				imin1 = i;
			}
			else if((a->tabNoeuds[i].parent == -1) && (a->tabNoeuds[i].nombreOccurences < min2))
			{
				min2 = a->tabNoeuds[i].nombreOccurences;
				imin2 = i;
			}
		} // On a trouvé les deux plus petits orphelins.
		
		a->tabNoeuds[nn].gauche = imin1;
		a->tabNoeuds[nn].droit = imin2;
		a->tabNoeuds[nn].nombreOccurences = (min1 + min2);
		a->tabNoeuds[nn].parent = -1;
		a->tabNoeuds[nn].c = 'i';
		
		a->tabNoeuds[imin1].parent = nn;
		a->tabNoeuds[imin2].parent = nn;
	}
	
	definirCouleurConsole(VERT);
	
	printf("Construction de l'arbre de Huffman termin%se.\n", eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	return a; // On retourne l'arbre.
}

void arbreAfficherCodes(Arbre* arbreAAfficher)
{
	for(int i=0;i<arbreAAfficher->taille;++i)
	{
		printf("i %d : gauche : %c, droit : %c, parent : %c, c : %c\n",
				i,
				arbreAAfficher->tabNoeuds[arbreAAfficher->tabNoeuds[i].gauche].c,
				arbreAAfficher->tabNoeuds[arbreAAfficher->tabNoeuds[i].droit].c,
				arbreAAfficher->tabNoeuds[arbreAAfficher->tabNoeuds[i].parent].c,
				arbreAAfficher->tabNoeuds[i].c);
	}
}

int estFeuille(Arbre* a, int i)
{
	return ((a->tabNoeuds[i].gauche == -1) && (a->tabNoeuds[i].droit == -1));
}

// --------- Manipulation des en-têtes de huffman ---------

int estDedans(CodeHuffman const* tab, uchar c)
{
	for(int i=0;i<tab[0].nombreFeuilles;++i)
	{
		if(tab[i].caractere == c)
			return 1;
	}
	
	return 0;
}

int recherche(CodeHuffman const* tab, uchar c)
{
	for(int i=0;i<tab[0].nombreFeuilles;++i)
	{
		if(tab[i].caractere == c)
			return i;
	}
	
	return -1;
}

void creerCodesHuffmanRecursif(Arbre* a, int t, int l, int* temp, CodeHuffman* codes)
{
	if(!(estFeuille(a, t)))
	{
		temp[l] = 0;
		creerCodesHuffmanRecursif(a, a->tabNoeuds[t].gauche, (l + 1), temp, codes);
		temp[l] = 1;
		creerCodesHuffmanRecursif(a, a->tabNoeuds[t].droit, (l + 1), temp, codes);
	}
	else
	{
		int chercheFin = 0;
		int chercheIndice = 0;
		
		while((codes[chercheIndice].code != NULL) && (chercheFin < ((a->taille + 1) / 2)))
		{
			++chercheFin;
			++chercheIndice;
		}
		
		codes[chercheIndice].nombreOccurences = a->tabNoeuds[t].nombreOccurences;
		codes[chercheIndice].caractere = a->tabNoeuds[t].c;
		
		codes[chercheIndice].code = (uchar*)malloc((sizeof(uchar) * l) + sizeof(uchar));
		
		for(int i=0;i<l;++i)
		{
			codes[chercheIndice].code[i] = (temp[i] + 48); // 48 étant le code ASCII en décimal du caractère "0".
		}
		codes[chercheIndice].code[l] = '\0';
	}
}

CodeHuffman* creerCodesHuffman(Arbre* a, int racine)
{
	definirCouleurConsole(VERT);
	
	printf("D%sbut de la cr%sation des codes de Huffman.\n", eAccAigu, eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	int* temp = (int*)malloc(sizeof(int) * 256);
	CodeHuffman* ch = (CodeHuffman*)malloc(sizeof(CodeHuffman) * ((a->taille + 1) / 2));
	
	for(int i=0;i<((a->taille + 1) / 2);++i)
		ch[i].code = NULL;
	
	creerCodesHuffmanRecursif(a, racine, 0, temp, ch);
	
	free(temp);
	
	for(int i=0;i<((a->taille + 1) / 2);++i)
		ch[i].nombreFeuilles = ((a->taille + 1) / 2);
	
	definirCouleurConsole(VERT);
	
	printf("Cr%sation des codes de Huffman termin%se.\n", eAccAigu, eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	return ch;
}

EnTeteHuffman* creerEnTeteHuffman(char* nomFichier, Arbre* a)
{
	definirCouleurConsole(VERT);
	
	printf("D%sbut de la cr%sation de l'en-t%ste du fichier compress%s.\n", eAccAigu, eAccAigu, eAccCirconflexe, eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	EnTeteHuffman* eth = (EnTeteHuffman*)malloc(sizeof(EnTeteHuffman));
	
	eth->nomOriginalFichier = (uchar*)malloc(sizeof(uchar) * strlen(nomFichier));
	
	strcpy((char*)eth->nomOriginalFichier, nomFichier);
	
	int taille = 0;
	int c;
	FILE* f = fopen((char*)eth->nomOriginalFichier, "r"); // Ouverture du fichier "chemin" en lecture.
	
	if(f == NULL)
	{
		definirCouleurConsole(ROUGE);
		
		fprintf(stderr, "Impossible d'ouvrir le fichier \"%s\" en mode lecture.\n", eth->nomOriginalFichier);
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	while((c = fgetc(f)) != EOF)
	{
		++taille;
	}
	
	fclose(f);
	
	eth->tailleOriginaleFichier = taille;
	
	int racine = 0;
	
	while(a->tabNoeuds[racine].parent != -1)
	{
		++racine;
	}
	
	eth->tabCorrespondance = creerCodesHuffman(a, racine);
	
	if(eth->tabCorrespondance[0].nombreFeuilles == 1)
	{
		eth->tabCorrespondance[0].code = malloc((sizeof(uchar) * 2));
		eth->tabCorrespondance[0].code[0] = '0';
		eth->tabCorrespondance[0].code[1] = '\0';
	}
	
	eth->nombreLignesCodes = (eth->tabCorrespondance[0].nombreFeuilles + ((estDedans(eth->tabCorrespondance, '\n')) ? 1 : 0));
	
	definirCouleurConsole(VERT);
	
	printf("Cr%sation de l'en-t%ste du fichier compress%s termin%se.\n", eAccAigu, eAccCirconflexe, eAccAigu, eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	return eth;
}

void afficherEnTeteHuffman(EnTeteHuffman const* eth)
{
	printf("Nom original du fichier : %s\n", eth->nomOriginalFichier);
	printf("Taille originale du fichier : %d\n", eth->tailleOriginaleFichier);
	printf("Nombre de bits compress%ss : %d\n", eAccAigu, eth->nombreBitsCompresse);
	printf("Nombre de lignes contenant la table de correspondance des caract%sres : %d\n", eAccGrave, eth->nombreLignesCodes);
	
	printf("Table de correspondance des caract%sres :\n", eAccGrave);
	
	for(int i=0;i<eth->tabCorrespondance[0].nombreFeuilles;++i)
	{
		printf("i : %d : nombreOccurences : %d, caractere : %c, code : %s, nombreFeuilles : %d\n",
				i,
				eth->tabCorrespondance[i].nombreOccurences,
				eth->tabCorrespondance[i].caractere,
				eth->tabCorrespondance[i].code,
				eth->tabCorrespondance[i].nombreFeuilles);
	}
}

void ecrireEnTeteHuffman(FILE* fichier, EnTeteHuffman const* eth)
{
	if(fichier == NULL)
	{
		definirCouleurConsole(ROUGE);
		
		fprintf(stderr, "ERREUR : Impossible d'ouvrir le fichier.\nErreur dans la fonction %s, %s la ligne %d, dans le fichier %s", FUNCTION_NAME, aAccGrave, __LINE__, __FILE__);
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	fprintf(fichier, "%s\n", eth->nomOriginalFichier);
	fprintf(fichier, "%d\n", eth->tailleOriginaleFichier);
	fprintf(fichier, "%d\n", eth->nombreBitsCompresse);
	fprintf(fichier, "%d\n", eth->nombreLignesCodes);
	
	for(int i=0;i<eth->tabCorrespondance[0].nombreFeuilles;++i)
		fprintf(fichier, "%c:%d\n", eth->tabCorrespondance[i].caractere, eth->tabCorrespondance[i].nombreOccurences);
}

// --------- Début des étapes de décompression de Huffman ---------

void supprimerUnCaractere(uchar* chaine, int position)
{
	int pos = 0;
	--position;
	
	while(chaine[pos] != '\0')
	{
		if(pos >= position)
			chaine[pos] = chaine[(pos + 1)];
		
		++pos;
	}
}

void supprimerCaracteres(uchar* chaine, int fin)
{
	for(int i=0;i<fin;++i)
	{
		supprimerUnCaractere(chaine, 0);
	}
}

EnTeteHuffman* lireEnTeteHuffman(uchar* contenuFichier)
{
	definirCouleurConsole(VERT);
	
	printf("D%sbut de la cr%sation de l'en-t%ste de Huffman.\n", eAccAigu, eAccAigu, eAccCirconflexe);
	
	definirCouleurConsole(NORMAL);
	
	int tailleAllocNOF = 0; // Taille de l'allocation pour le nom original du fichier dans l'en-tête.
	int tailleAllocTOF = 0; // Taille de l'allocation pour la taille originale du fichier dans l'en-tête.
	int tailleAllocNBC = 0; // Taille de l'allocation pour le nombre de bits compressés du fichier dans l'en-tête.
	int tailleAllocNLC = 0; // Taille de l'allocation pour le nombre de lignes des codes du fichier dans l'en-tête.
	EnTeteHuffman* eth = (EnTeteHuffman*)malloc(sizeof(EnTeteHuffman));
	
	// --- Nom original du fichier ---
	// - Taille de l'allocation -
	while(contenuFichier[tailleAllocNOF] != '\n')
		++tailleAllocNOF;
	
	++tailleAllocNOF;
	// - Fin taille de l'allocation -
	
	eth->nomOriginalFichier = (uchar*)malloc((sizeof(uchar) * tailleAllocNOF)); // - Allocation -
	
	// - Recopie -
	for(int i=0;i<(tailleAllocNOF - 1);++i)
		eth->nomOriginalFichier[i] = contenuFichier[i];
	
	eth->nomOriginalFichier[(tailleAllocNOF - 1)] = '\0';
	// - Fin recopie -
	
	supprimerCaracteres(contenuFichier, tailleAllocNOF);
	// --- Fin nom original du fichier ---
	
	// --- Taille originale du fichier ---
	// - Taille de l'allocation -
	while(contenuFichier[tailleAllocTOF] != '\n')
		++tailleAllocTOF;
	
	++tailleAllocTOF;
	// - Fin taille de l'allocation -
	
	uchar* tailleOrFic = (uchar*)malloc((sizeof(uchar) * tailleAllocTOF)); // - Allocation -
	
	// - Recopie -
	for(int i=0;i<(tailleAllocTOF - 1);++i)
		tailleOrFic[i] = contenuFichier[i];
	
	tailleOrFic[(tailleAllocTOF - 1)] = '\0';
	
	eth->tailleOriginaleFichier = atoi((char*)tailleOrFic);
	// - Fin recopie -
	
	free(tailleOrFic); // - Désallocation -
	
	supprimerCaracteres(contenuFichier, tailleAllocTOF);
	// --- Fin taille originale du fichier ---
	
	// --- Nombre de bits compressés du fichier ---
	// - Taille de l'allocation -
	while(contenuFichier[tailleAllocNBC] != '\n')
		++tailleAllocNBC;
	
	++tailleAllocNBC;
	// - Fin taille de l'allocation -
	
	uchar* nombreBitsComp = (uchar*)malloc((sizeof(uchar) * tailleAllocNBC)); // - Allocation -
	
	// - Recopie -
	for(int i=0;i<(tailleAllocNBC - 1);++i)
		nombreBitsComp[i] = contenuFichier[i];
	
	nombreBitsComp[(tailleAllocNBC - 1)] = '\0';
	
	eth->nombreBitsCompresse = atoi((char*)nombreBitsComp);
	// - Fin recopie -
	
	free(nombreBitsComp); // - Désallocation -
	
	supprimerCaracteres(contenuFichier, tailleAllocNBC);
	// --- Fin nombre de bits compressés du fichier ---
	
	// --- Nombre de lignes des codes du fichier ---
	// - Taille de l'allocation -
	while(contenuFichier[tailleAllocNLC] != '\n')
		++tailleAllocNLC;
	
	++tailleAllocNLC;
	// - Fin taille de l'allocation -
	
	uchar* nombreLigCdes = (uchar*)malloc((sizeof(uchar) * tailleAllocNLC)); // - Allocation -
	
	// - Recopie -
	for(int i=0;i<(tailleAllocNLC - 1);++i)
		nombreLigCdes[i] = contenuFichier[i];
	
	nombreLigCdes[(tailleAllocNLC - 1)] = '\0';
	
	eth->nombreLignesCodes = atoi((char*)nombreLigCdes);
	// - Fin recopie -
	
	free(nombreLigCdes); // - Désallocation -
	
	supprimerCaracteres(contenuFichier, tailleAllocNLC);
	// --- Fin nombre de lignes des codes du fichier ---
	
	eth->tabCorrespondance = lireCodesHuffman(contenuFichier, eth->nombreLignesCodes);
	
	definirCouleurConsole(VERT);
	
	printf("Cr%sation de l'en-t%ste de Huffman termin%se.\n", eAccAigu, eAccCirconflexe, eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	return eth;
}

CodeHuffman* lireCodesHuffman(uchar* contenuFichierReduit, int nombreLignesCodes)
{
	definirCouleurConsole(VERT);
	
	printf("D%sbut de la cr%sation des codes de Huffman.\n", eAccAigu, eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	int taille = 0;
	int i = 0;
	
	while(i<nombreLignesCodes)
	{
		if(contenuFichierReduit[taille] == '\n')
			++i;
		
		++taille;
	}
	
	uchar* tab = (uchar*)malloc(sizeof(uchar) * taille);
	
	for(int i=0;i<taille;++i)
		tab[i] = contenuFichierReduit[i];
	
	supprimerCaracteres(contenuFichierReduit, taille);
	
	int nb = 0;
	int calculRetourChariot = 0;
	
	for(int i=0;i<taille;++i)
	{
		if((tab[i] == '\n') && (calculRetourChariot == 1))
			nb = i;
		else
		{
			if(tab[i] == '\n')
				++calculRetourChariot;
			else
				calculRetourChariot = 0;
		}
	}
	
	nb = (nb ? nb : nb);
	
	uchar* tmp = (uchar*)malloc((sizeof(uchar) * taille));
	strcpy((char*)tmp, (char*)tab);
	
	int cmptIteration = 0;
	
	CodeHuffman* tabCh = (CodeHuffman*)malloc((sizeof(CodeHuffman) * nombreLignesCodes));
	
	while(strlen((char*)tab) > 0)
	{
		int compteur = 0;
		
		while(tab[compteur] != '\n')
			++compteur;
		
		if(((tab[compteur] == '\n') && (tab[(compteur - 1)] == '\n')) || (compteur == 0))
		{
			++compteur;
			
			while(tab[compteur] != '\n')
				++compteur;
			
			++compteur;
		}
		else
			++compteur;
		
		uchar* temporaire = (uchar*)malloc((sizeof(uchar) * compteur));
		
		for(int i=0;i<compteur;++i)
		{
			temporaire[i] = tab[i];
		}
		
		supprimerCaracteres(tab, compteur);
		
		CodeHuffman ch;
		
		ch.caractere = temporaire[0];
		
		supprimerCaracteres(temporaire, 2);
		
		uchar* nbOcc = (uchar*)malloc((sizeof(uchar) * (compteur - 2)));
		
		for(int i=0;i<(compteur - 2);++i)
			nbOcc[i] = temporaire[i];
		
		ch.nombreOccurences = atoi((char*)nbOcc);
		
		free(nbOcc);
		
		tabCh[cmptIteration].caractere = ch.caractere;
		tabCh[cmptIteration].nombreOccurences = ch.nombreOccurences;
		
		++cmptIteration;
	}
	
	for(int i=0;i<cmptIteration;++i)
	{
		tabCh[i].nombreFeuilles = cmptIteration;
	}
	
	definirCouleurConsole(VERT);
	
	printf("Cr%sation des codes de Huffman termin%se.\n", eAccAigu, eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	return tabCh;
}

Arbre *creerArbreDepuisEnTeteHuffman(EnTeteHuffman* eth)
{
	definirCouleurConsole(VERT);
	
	printf("D%sbut de la construction de l'arbre de Huffman.\n", eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	int taille = eth->tailleOriginaleFichier;
	
	Arbre* a = (Arbre*)malloc(sizeof(Arbre));
	
	a->taille = ((eth->tabCorrespondance[0].nombreFeuilles * 2) - 1);
	a->tabNoeuds = (Noeud*)malloc(sizeof(Noeud) * a->taille);
	
	int indiceTab = 0; // Indice dans le tableau.
	
	for(int i=0;i<eth->tabCorrespondance[0].nombreFeuilles;++i)
	{
		if(eth->tabCorrespondance[i].nombreOccurences != 0)
		{
			a->tabNoeuds[indiceTab].c = eth->tabCorrespondance[i].caractere; // Caractère que l'on parcours.
			a->tabNoeuds[indiceTab].nombreOccurences = eth->tabCorrespondance[i].nombreOccurences; // Nombre d'occurences.
			a->tabNoeuds[indiceTab].gauche = -1;
			a->tabNoeuds[indiceTab].droit = -1;
			a->tabNoeuds[indiceTab].parent = -1;
			++indiceTab;
		}
	}
	
	for(int nn=indiceTab;nn<a->taille;++nn)
	{
		// Choix des 2 plus petits noeuds orphelin (ceux qui ont le plus petit nombreOccurences).
		int min1 = taille;
		int min2 = taille;
		int imin1 = min1;
		int imin2 = min2;
		
		for(int i=0;i<nn;++i)
		{
			if((a->tabNoeuds[i].parent == -1) && (a->tabNoeuds[i].nombreOccurences < min1))
			{
				min2 = min1; // L'ancien plus petit devient le deuxième.
				imin2 = imin1;
				
				min1 = a->tabNoeuds[i].nombreOccurences;
				imin1 = i;
			}
			else if((a->tabNoeuds[i].parent == -1) && (a->tabNoeuds[i].nombreOccurences < min2))
			{
				min2 = a->tabNoeuds[i].nombreOccurences;
				imin2 = i;
			}
		} // On a trouvé les deux plus petits orphelins.
		
		a->tabNoeuds[nn].gauche = imin1;
		a->tabNoeuds[nn].droit = imin2;
		a->tabNoeuds[nn].nombreOccurences = (min1 + min2);
		a->tabNoeuds[nn].parent = -1;
		a->tabNoeuds[nn].c = 'i';
		
		a->tabNoeuds[imin1].parent = nn;
		a->tabNoeuds[imin2].parent = nn;
	}
	
	definirCouleurConsole(VERT);
	
	printf("Construction de l'arbre de Huffman termin%se.\n", eAccAigu);
	
	definirCouleurConsole(NORMAL);
	
	return a;
}
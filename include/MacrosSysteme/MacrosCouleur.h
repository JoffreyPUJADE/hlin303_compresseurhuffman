#ifndef MACROSCOULEUR_H
#define MACROSCOULEUR_H

/*! \page page6 MacrosCouleur
 * \par Description du fichier \a "MacrosCouleur.h" \a 
 * 		Le fichier \a "MacrosCouleur.h" \a définit différentes macros ainsi qu'une fonction pour écrire en
 * 		couleur dans la console.
 * 		
 * 		Les macros définies dans ce fichier sont :
 * 			- NOIR : Couleur noire
 * 			- BLEU : Couleur bleue
 * 			- VERT : Couleur verte
 * 			- CYAN : Couleur cyan
 * 			- ROUGE : Couleur rouge
 * 			- MAGENTA : Couleur magenta
 * 			- JAUNE : Couleur jaune
 * 			- BLANC : Couleur blanche
 * 			- NORMAL : Couleur normale de la console.
 * 		
 * 		La fonction portable (Windows/Linux) définie dans ce fichier est ::definirCouleurConsole(int couleur).
 * \warning ATTENTION : L'utilisation de ces macros nécessitent, sous Windows, l'emploi du drapeau de
 * 						compilation "-lkernel32" lors de l'édition des liens.
 */

/*! \file MacrosCouleur.h
 * \brief Contient tout le nécessaire pour écrire en couleur.
 * \details Ce fichier définit différentes macros ainsi qu'une fonction pour écrire en couleur dans la console.
 * 			Les macros définies dans ce fichier sont :
 * 				- NOIR : Couleur noire
 * 				- BLEU : Couleur bleue
 * 				- VERT : Couleur verte
 * 				- CYAN : Couleur cyan
 * 				- ROUGE : Couleur rouge
 * 				- MAGENTA : Couleur magenta
 * 				- JAUNE : Couleur jaune
 * 				- BLANC : Couleur blanche
 * 				- NORMAL : Couleur normale de la console.
 * 			La fonction portable (Windows/Linux) définie dans ce fichier est ::definirCouleurConsole(int couleur).
 * \warning ATTENTION : L'utilisation de ces macros nécessitent, sous Windows, l'emploi du drapeau de
 * 						compilation "-lkernel32" lors de l'édition des liens.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "MacrosOS.h"

#include <stdio.h>
#include <string.h>

#if defined(WINDOWS)
	#include <windows.h>
#endif

#if defined(WINDOWS)
	#define NOIR 0
	#define BLEU 1
	#define VERT 2
	#define CYAN 3
	#define ROUGE 4
	#define MAGENTA 5
	#define JAUNE 14
	#define BLANC 15
	#define NORMAL 15
#elif defined(POSIX)
	#define NOIR 0
	#define ROUGE 1
	#define VERT 2
	#define JAUNE 3
	#define BLEU 4
	#define MAGENTA 5
	#define CYAN 6
	#define BLANC 7
	#define NORMAL 7
#endif

#if defined(WINDOWS)
	/*! \fn void setCouleur(int couleur)
	 * \brief Écrit en couleur.
	 * \param [in] couleur (int) : Valeur décimale de la couleur.
	 * \details Cette procédure permet d'écrire en couleur dans la console sous Windows.
	 * \callergraph
	 * \callgraph
	 */
	void setCouleur(int couleur);
#elif defined(POSIX)
	/*! \fn void setCouleur(int couleur)
	 * \brief Écrit en couleur.
	 * \param [in] attribut (int) : Valeur d'affichage de la couleur.
	 * \param [in] couleurPremierPlan (int) : Valeur décimale de la couleur au premier plan.
	 * \param [in] couleurSecondPlan (int) : Valeur décimale de la couleur au second plan.
	 * \details Cette procédure permet d'écrire en couleur dans la console sous un système
	 * 			POSIX (sous Linux, de préférence).
	 * \callergraph
	 * \callgraph
	 */
	void colorer(int attribut, int couleurPremierPlan, int couleurSecondPlan);
	
	/*! \fn void setCouleur(int couleur)
	 * \brief Écrit en couleur.
	 * \param [in] couleur (int) : Valeur décimale de la couleur.
	 * \details Cette procédure permet d'écrire en couleur plus facilement dans la console
	 * 			sous un système POSIX (sous Linux, de préférence).
	 * \callergraph
	 * \callgraph
	 */
	void defCouleur(int couleur);
#endif

/*! \fn void setCouleur(int couleur)
 * \brief Écrit en couleur.
 * \param [in] couleur (int) : Valeur décimale de la couleur.
 * \details Cette procédure permet d'écrire en couleur dans la console sous Windows ou sous
 * 			un système POSIX (sous Linux, de préférence) de manière portable entre les deux
 * 			systèmes d'exploitation.
 * \callergraph
 * \callgraph
 */
void definirCouleurConsole(int couleur);

#endif // MACROSCOULEUR_H
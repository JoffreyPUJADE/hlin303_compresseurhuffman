#ifndef MANIP_H
#define MANIP_H

/*! \page page1 Manip
 * \par Description du fichier \a "Manip.h" \a 
 * 		Le fichier \a "Manip.h" \a contient le nécessaire pour réaliser des manipulation de base d'un fichier.
 * 		
 * 		Il contient l'alias de type ::uchar.
 * 		
 * 		Il contient également les fonctions suivantes :
 * 			- ::allouerChaine(int tailleAlloc) ;
 * 			- ::compterCarFic(char* nomFichier) ;
 * 			- ::fichierVersChaine(char* nomFichier, int* tailleChaine).
 */

/*! \file Manip.h
 * \brief Contient tout le nécessaire pour manipuler un fichier.
 * \details Ce fichier contient l'alias de type "uchar", ainsi que les fonctions nécessaires pour lire un fichier et l'enregistrer
 * 			dans une chaîne de caractères, afin de le manipuler par la suite.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "MacrosSysteme/MacrosSysteme.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>

/*! \typedef unsigned char uchar;
 * \brief Alias de type.
 * \details Cet alias définit le type "uchar" à utiliser en lieu et place du type "unsigned char".
 */
typedef unsigned char uchar;

/*! \fn uchar* allouerChaine(int tailleAlloc)
 * \brief Alloue une chaîne de caractères.
 * \param [in] tailleAlloc (int) : Taille de l'allocation de mémoire.
 * \details Cette fonction alloue une chaîne de caractères.
 * \return Chaîne de caractères allouée (uchar*).
 * \callergraph
 * \callgraph
 */
uchar* allouerChaine(int tailleAlloc);

/*! \fn int compterCarFic(char* nomFichier)
 * \brief Compte les caractères.
 * \param [in] nomFichier (char const*) : Nom du fichier à lire.
 * \details Cette fonction compte les caractères d'un fichier.
 * \return Nombre de caractères contenu dans le fichier (int).
 * \callergraph
 * \callgraph
 */
int compterCarFic(char const* nomFichier);

/*! \fn uchar* fichierVersChaine(char* nomFichier, int* tailleChaine)
 * \brief Enregistre un fichier dans une chaîne.
 * \param [in] nomFichier (char const*) : Nom du fichier à lire.
 * \param [inout] tailleChaine (int*) : Taille de la chaine de caractère.
 * \details Cette fonction permet de lire un fichier et d'en enregistrer le contenu dans une chaîne de caractères.
 * \warning ATTENTION : Le pointeur sur la taille de la chaîne de caractères NE DOIT PAS être un pointeur nul (NULL)
 * 			et DOIT ÊTRE alloué et la valeur pointée doit être égale à 0.
 * \return Fichier enregistré dans la chaîne de caractères (uchar*).
 * \callergraph
 * \callgraph
 */
uchar* fichierVersChaine(char const* nomFichier, int* tailleChaine);

/*! \fn int estVide(char const* nomFichier)
 * \brief Vérifie un fichier.
 * \param [in] nomFichier (char const*) : Nom du fichier à vérifier.
 * \details Cette fonction permet de vérifier si un fichier est vide ou non.
 * \return Vérification du fichier (int) [1 si c'est vide ; 0 sinon].
 * \callergraph
 * \callgraph
 */
int estVide(char const* nomFichier);

#endif // MANIP_H
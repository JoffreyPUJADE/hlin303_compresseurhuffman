#include "../include/MacrosSysteme/MacrosCouleur.h"

#if defined(WINDOWS)
	void setCouleur(int couleur)
	{
		HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
		
		SetConsoleTextAttribute(h, FOREGROUND_INTENSITY | couleur);
	}
#elif defined(POSIX)
	void colorer(int attribut, int couleurPremierPlan, int couleurSecondPlan)
	{
		char commande[13];
		
		sprintf(commande, "%c[%d;%hu;%hum", 0x1B, attribut, (unsigned short)(couleurPremierPlan + 30), (unsigned short)(couleurSecondPlan + 40));
		
		printf("%s", commande);
	}
	
	void defCouleur(int couleur)
	{
		#define RESET 0
		#define BRILLANT 1
		#define DIM 2
		#define SOULIGNE 3
		#define BLINK 4
		#define RETOURNE 7
		#define CACHE 8
		
		if(couleur == NORMAL)
			colorer(RESET, NORMAL, NOIR);
		else
			colorer(BRILLANT, couleur, NOIR);
	}
#endif

void definirCouleurConsole(int couleur)
{
	#if defined(WINDOWS)
		setCouleur(couleur);
	#elif defined(POSIX)
		defCouleur(couleur);
	#endif
}
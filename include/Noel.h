#ifndef NOEL_H
#define NOEL_H

/*! \page page12 Noel
 * \par Description du fichier \a "Noel.h" \a 
 * 		Le fichier \a "Noel.h" \a contient le nécessaire pour réaliser un message des développeurs pour les fêtes de
 * 		fin d'années 2020.
 * 		
 * 		Il contient les fonctions suivantes :
 * 			- ::estPair(int nb)
 * 			- ::dessinerPartieSapin(int nombreLignes, int sautALaLigne)
 * 			- ::dessinerPartieTronc(int largeurSapin, int hauteurTronc)
 * 			- ::dessinerSapin()
 * 			- ::dessinBonnesFetes()
 * 			- ::messageDesDeveloppeurs()
 * 			- ::etMaintenantUnMessageDesDeveloppeurs()
 */

/*! \file Noel.h
 * \brief Contient tout le nécessaire les fêtes.
 * \details Ce fichier contient le nécessaire pour réaliser un message des développeurs pour les fêtes de fin d'années 2020.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "MacrosSysteme/MacrosSysteme.h"

/*! \fn int estPair(int nb)
 * \brief Vérifie si un nombre est pair.
 * \param [in] nb (int) : Nombre à vérifier.
 * \details Cette fonction permet de vérifier si un nombre est pair ou non.
 * \return Vérification (int) [1 si c'est pair ; 0 sinon].
 * \callergraph
 * \callgraph
 */
int estPair(int nb);

/*! \fn void dessinerPartieSapin(int nombreLignes, int sautALaLigne)
 * \brief Dessine une partie d'un sapin.
 * \param [in] nombreLignes (int) : Nombre de lignes sur lesquelles la partie de sapin sera dessinée.
 * \param [in] sautALaLigne (int) : Définit si un saut à la ligne doit avoir lieu ou non.
 * \details Cette procédure permet de dessiner une partie de sapin de Noël dans la console.
 * \callergraph
 * \callgraph
 */
void dessinerPartieSapin(int nombreLignes, int sautALaLigne);

/*! \fn void dessinerPartieTronc(int largeurSapin, int hauteurTronc)
 * \brief Dessine un tronc de sapin.
 * \param [in] largeurSapin (int) : Nombre de colonnes sur lesquelles le tronc de sapin sera dessiné.
 * \param [in] hauteurTronc (int) : Nombre de lignes sur lesquelles le tronc de sapin sera dessiné.
 * \details Cette procédure permet de dessiner un tronc de sapin de Noël dans la console.
 * \callergraph
 * \callgraph
 */
void dessinerPartieTronc(int largeurSapin, int hauteurTronc);

/*! \fn void dessinerSapin()
 * \brief Dessine un sapin.
 * \details Cette procédure permet de dessiner un sapin de Noël dans la console.
 * \callergraph
 * \callgraph
 */
void dessinerSapin();

/*! \fn void dessinBonnesFetes()
 * \brief Dessine "Bonnes fêtes 2020".
 * \details Cette procédure permet de dessiner "Bonnes fêtes 2020" dans la console
 * \callergraph
 * \callgraph
 */
void dessinBonnesFetes();

/*! \fn void messageDesDeveloppeurs()
 * \brief Dessine un mot de Noël.
 * \details Cette procédure permet de dessiner un mot de Noël des développeurs dans la console
 * \callergraph
 * \callgraph
 */
void messageDesDeveloppeurs();

/*! \fn void etMaintenantUnMessageDesDeveloppeurs()
 * \brief Dessine un mot de Noël mis en forme.
 * \details Cette procédure permet de dessiner un mot de Noël des développeurs mis en forme dans la console
 * \callergraph
 * \callgraph
 */
void etMaintenantUnMessageDesDeveloppeurs();

#endif // NOEL_H
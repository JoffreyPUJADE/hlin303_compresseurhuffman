#ifndef ARBRE_H
#define ARBRE_H

/*! \page page9 Arbre
 * \par Description du fichier \a "Arbre.h" \a 
 * 		Le fichier \a "Arbre.h" \a contient le nécessaire pour représenter et manipuler un Arbre de Huffman, les codes
 * 		les en-têtes nécessaires à la compression (et à la décompression) de Huffman.
 * 		
 * 		Il contient les alias de types suivants :
 * 			- ::Arbre ;
 * 			- ::CodeHuffman ;
 * 			- ::EnTeteHuffman.
 * 		
 * 		Il contient les strucutres suivantes :
 * 			- ::Arbre ;
 * 			- ::CodeHuffman ;
 * 			- ::EnTeteHuffman.
 * 		
 * 		Il contient également les fonctions suivantes :
 * 			- ::creerArbre(char* chemin) ;
 * 			- ::arbreAfficherCodes(Arbre* arbreAAfficher) ;
 * 			- ::estFeuille(Arbre* a, int i) ;
 * 			- ::estDedans(CodeHuffman const* tab, uchar c) ;
 * 			- ::recherche(CodeHuffman const* tab, uchar c) ;
 * 			- ::creerCodesHuffmanRecursif(Arbre* a, int t, int l, int* temp, CodeHuffman* codes) ;
 * 			- ::creerCodesHuffman(Arbre* a, int racine) ;
 * 			- ::creerEnTeteHuffman(char* nomFichier, Arbre* a) ;
 * 			- ::afficherEnTeteHuffman(EnTeteHuffman const* eth) ;
 * 			- ::ecrireEnTeteHuffman(FILE* fichier, EnTeteHuffman const* eth) ;
 * 			- ::supprimerUnCaractere(uchar* chaine, int position) ;
 * 			- ::supprimerCaracteres(uchar* chaine, int fin) ;
 * 			- ::lireEnTeteHuffman(uchar* contenuFichier) ;
 * 			- ::lireCodesHuffman(uchar* contenuFichierReduit, int nombreLignesCodes) ;
 * 			- ::creerArbreDepuisEnTeteHuffman(EnTeteHuffman* eth).
 */

/*! \file Arbre.h
 * \brief Contient tout le nécessaire pour un arbre de Huffman.
 * \details Ce fichier contient le nécessaire pour représenter et manipuler un Arbre de Huffman, les codes les
 * 			en-têtes nécessaires à la compression (et à la décompression) de Huffman.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "Noeud.h"
#include "MacrosSysteme/MacrosSysteme.h"
#include "Manip.h"
#include <stdio.h>

/*! \struct Arbre
 * \brief Structure d'un arbre de Huffman.
 * \details Cette structure définit ce que contient un arbre de Huffman. Ici, un arbre est caractérisé par un tableau de noeuds
 *  		et la taille dudit tableau.
 */
struct Arbre
{
	Noeud* tabNoeuds; /*!< Cette variable est le tableau de la totalité des noeuds composant l'arbre de Huffman. */
	int taille; /*!< Cette variable contient la taille du tableau de noeuds. */
};

/*! \struct CodeHuffman
 * \brief Structure d'un code de Huffman.
 * \details Cette structure définit un code de Huffman ; il contient le nombre d'occurences d'un caractère, le caractère en
 * 			lui-même, le nombre de feuilles pour faciliter le parcours de la table de correspondance et le code lui-même s'il
 * 			est nécessaire.
 */
struct CodeHuffman
{
	int nombreOccurences; /*!< Cette variable contient le nombre d'occurences du caractère. */
	uchar caractere; /*!< Cette variable contient le caractère en lui-même. */
	uchar* code; /*!< Cette variable contient le code du caractère. */
	int nombreFeuilles; /*!< Cette variable contient le nombre de feuilles de l'arbre dont les codes sont issus ; nécessaire lors de l'emploi de la structure "EnTeteHuffman". */
};

/*! \struct EnTeteHuffman
 * \brief Structure d'une en-tête de fichier.
 * \details Cette structure définit une en-tête d'un fichier compressé ; elle contient le nom et la taille originale du fichier
 * 			source, le nombre de bits compressés, le nombre de lignes constituant la table de correspondance de caractères ainsi
 * 			que ladite table en elle-même.
 */
struct EnTeteHuffman
{
	uchar* nomOriginalFichier; /*!< Cette variable contient le nom original du fichier source. */
	int tailleOriginaleFichier; /*!< Cette variable contient la taille originale du fichier source. */
	int nombreBitsCompresse; /*!< Cette variable contient le nombre de bits compressés. */
	int nombreLignesCodes; /*!< Cette variable contient le nombre de lignes nécessaire à l'écriture de la table de correspondance des caractères dans le fichier compressé. */
	struct CodeHuffman* tabCorrespondance; /*!< Cette variable est la table de correspondance des caractères en elle-même. */
};

/*! \typedef struct Arbre Arbre;
 * \brief Alias de structure.
 * \details Cet alias définit le type "Arbre" à utiliser en lieu et place du type "struct Arbre".
 */
typedef struct Arbre Arbre;

/*! \typedef struct CodeHuffman CodeHuffman;
 * \brief Alias de structure.
 * \details Cet alias définit le type "CodeHuffman" à utiliser en lieu et place du type "struct CodeHuffman".
 */
typedef struct CodeHuffman CodeHuffman;

/*! \typedef struct EnTeteHuffman EnTeteHuffman;
 * \brief Alias de structure.
 * \details Cet alias définit le type "EnTeteHuffman" à utiliser en lieu et place du type "struct EnTeteHuffman".
 */
typedef struct EnTeteHuffman EnTeteHuffman;

/*! \fn Noeud *creerArbre(uchar* chemin)
 * \brief Crée un arbre de Huffman de manière sécurisée.
 * \param [inout] chemin (uchar*) : Chemin du fichier à compresser.
 * \details Cette fonction permet la création de manière sûre un arbre de Huffman.
 * \note Cette fonction est destinée à être appelée lors de la compression d'un fichier via Huffman.
 * \return Arbre créé (Arbre*).
 * \callergraph
 * \callgraph
 */
Arbre *creerArbre(char* chemin);

/*! \fn void arbreAfficherCodes(Arbre* arbreAAfficher)
 * \brief Affiche un arbre.
 * \param [inout] arbreAAfficher (Arbre*) : Arbre à afficher.
 * \details Cette procédure parcourt l'arbre passé en paramètre pour l'afficher dans la console.
 * \callergraph
 * \callgraph
 */
void arbreAfficherCodes(Arbre* arbreAAfficher);

/*! \fn int estFeuille(Arbre* a, int i)
 * \brief Vérifie une feuille.
 * \param [inout] a (Arbre*) : Arbre dont on doit vérifier l'un des noeuds.
 * \param [in] i (int) : Position du noeud à vérifier.
 * \details Cette fonction permet de vérifier si le noeud, à la position donnée en paramètre, de l'arbre, donné
 * 			en paramètre, est une feuille ou non.
 * \return Vérification du noeud (int) [1 si c'est une feuille ; 0 sinon].
 * \callergraph
 * \callgraph
 */
int estFeuille(Arbre* a, int i);

// --------- Manipulation des en-têtes de huffman ---------

/*! \fn int estDedans(CodeHuffman* tab, uchar c)
 * \brief Vérifie un caractère.
 * \param [in] tab (CodeHuffman const*) : Table de correspondance caractère-code de la vérification.
 * \param [in] c (uchar) : Caractère à vérifier dans la table de correspondance caractère-code.
 * \details Cette fonction permet de vérifier la présence d'un caractère dans la table de correspondance caractère-code.
 * \return Vérification de la présence (int) [1 si c'est présent ; 0 sinon].
 * \callergraph
 * \callgraph
 */
int estDedans(CodeHuffman const* tab, uchar c);

/*! \fn int recherche(CodeHuffman* tab, uchar c)
 * \brief Cherche une position.
 * \param [in] tab (CodeHuffman const*) : Table de correspondance caractère-code de la recherche.
 * \param [in] c (uchar) : Caractère à rechercher dans la table de correspondance caractère-code.
 * \details Cette fonction permet de rechercher la position d'un caractère dans la table de correspondance caractère-code.
 * \return Position du caractère si trouvé, -1 sinon (int).
 * \callergraph
 * \callgraph
 */
int recherche(CodeHuffman const* tab, uchar c);

/*! \fn void creerCodesHuffmanRecursif(Arbre* a, int t, int l, int* temp, CodeHuffman* codes)
 * \brief Création récursive des codes.
 * \param [inout] a (Arbre*) : Arbre à parcourir pour déterminer les codes.
 * \param [in] t (int) : Position "courante" dans l'arbre lors du parcours.
 * \param [in] l (int) : Nombre de bits qu'occupe le code en cours de création.
 * \param [inout] temp (int*) : Code en cours de création.
 * \param [inout] codes (CodeHuffman*) : Table de correspondance caractère-code en cours de création.
 * \details Cette procédure récursive permet la création de la table de correspondance caractère-code nécessaire à
 * 			l'en-tête de Huffman en cours de création et à la compression du fichier.
 * \note Cette procédure est destinée à être appelée lors de la compression d'un fichier via Huffman.
 * \callergraph
 * \callgraph
 */
void creerCodesHuffmanRecursif(Arbre* a, int t, int l, int* temp, CodeHuffman* codes);

/*! \fn CodeHuffman* creerCodesHuffman(Arbre* a, int racine)
 * \brief Création des codes.
 * \param [inout] a (Arbre*) : Arbre à parcourir pour déterminer les codes.
 * \param [in] racine (int) : Racine de l'arbre à parcourir.
 * \details Cette fonction permet la création de la table de correspondance caractère-code nécessaire à l'en-tête
 * 			de Huffman en cours de création et à la compression du fichier. Elle réalise le premier appel de la
 * 			fonction ::creerCodesHuffmanRecursif(Arbre* a, int t, int l, int* temp, CodeHuffman* codes).
 * \note Cette fonction est destinée à être appelée lors de la compression d'un fichier via Huffman.
 * \return Table de correspondance caractère-code créée (CodeHuffman*).
 * \callergraph
 * \callgraph
 */
CodeHuffman* creerCodesHuffman(Arbre* a, int racine);

/*! \fn EnTeteHuffman* creerEnTeteHuffman(char* nomFichier, Arbre* a)
 * \brief Création de l'en-tête.
 * \param [inout] nomFichier (char*) : Nom du fichier source destiné à l'en-tête.
 * \param [inout] a (Arbre*) : Arbre dont on doit extraire des informations pour l'en-tête.
 * \details Cette fonction permet la création d'une en-tête de Huffman à écrire dans le fichier compressé lors de la
 * 			compression.
 * \note Cette fonction est destinée à être appelée lors de la compression d'un fichier via Huffman.
 * \return En-tête de Huffman créée (EnTeteHuffman*).
 * \callergraph
 * \callgraph
 */
EnTeteHuffman* creerEnTeteHuffman(char* nomFichier, Arbre* a);

/*! \fn void afficherEnTeteHuffman(EnTeteHuffman* eth)
 * \brief Affiche une en-tête.
 * \param [in] eth (EnTeteHuffman const*) : En-tête à afficher.
 * \details Cette procédure permet d'afficher une en-tête de Huffman dans la console.
 * \callergraph
 * \callgraph
 */
void afficherEnTeteHuffman(EnTeteHuffman const* eth);

/*! \fn void ecrireEnTeteHuffman(FILE* fichier, EnTeteHuffman* eth)
 * \brief Écrit une en-tête.
 * \param [inout] fichier (FILE*) : Flux vers le fichier dans lequel écrire.
 * \param [in] eth (EnTeteHuffman const*) : En-tête à écrire dans le fichier.
 * \details Cette procédure permet d'écrire une en-tête de Huffman dans un fichier compressé.
 * \note Cette procédure est destinée à être appelée lors de la compression d'un fichier via Huffman.
 * \callergraph
 * \callgraph
 */
void ecrireEnTeteHuffman(FILE* fichier, EnTeteHuffman const* eth);

// --------- Début des étapes de décompression de Huffman ---------

/*! \fn void supprimerUnCaractere(uchar* chaine, int position)
 * \brief Retire un caractère d'une chaîne.
 * \param [inout] chaine (uchar*) : Chaîne de caractères dont on doit retirer un caractère.
 * \param [in] position (int) : Position du caractère à retirer.
 * \details Cette procédure permet de retirer un caractère d'une chaîne à une position donnée.
 * \callergraph
 * \callgraph
 */
void supprimerUnCaractere(uchar* chaine, int position);

/*! \fn void supprimerCaracteres(uchar* chaine, int fin)
 * \brief Retire plusieurs caractères d'une chaîne.
 * \param [inout] chaine (uchar*) : Chaîne de caractères dont on doit retirer des caractères.
 * \param [in] fin (int) : Position de fin de retrait de caractères.
 * \details Cette procédure permet de retirer plusieurs caractères d'une chaîne jusqu'à une
 * 			position donnée.
 * \callergraph
 * \callgraph
 */
void supprimerCaracteres(uchar* chaine, int fin);

/*! \fn EnTeteHuffman* lireEnTeteHuffman(uchar* contenuFichier)
 * \brief Lit une en-tête depuis un fichier.
 * \param [inout] contenuFichier (uchar*) : Chaîne de caractères contenant le fichier compressé.
 * \details Cette fonction permet de créer une en-tête de Huffman depuis une chaîne de caractères contenant
 * 			le fichier compressé. La chaîne de caracteres contenant le fichier compressé est réduite à chaque
 * 			ligne spécifique à l'en-tête (nom et taille originale du fichier, ...) lue.
 * \note Cette fonction est destinée à être appelée lors de la décompression d'un fichier via Huffman.
 * \return En-tête lue et créée (EnTeteHuffman*).
 * \callergraph
 * \callgraph
 */
EnTeteHuffman* lireEnTeteHuffman(uchar* contenuFichier);

/*! \fn CodeHuffman* lireCodesHuffman(uchar* contenuFichierReduit, int nombreLignesCodes)
 * \brief Lit les codes depuis un fichier.
 * \param [inout] contenuFichierReduit (uchar*) : Chaîne de caractères contenant le fichier compressé sans les lignes spécifiques à l'en-tête.
 * \param [in] nombreLignesCodes (int) : Nombre de ligne qu'occupe la table de correspondance caractere-occurrences dans le fichier.
 * \details Cette fonction permet de créer la table de correspondance caractere-occurrences depuis une chaîne de caractères contenant
 * 			le fichier compressé, sans les lignes spécifiques à l'en-tête (nom et taille originale du fichier, ...). La chaîne de
 * 			caracteres contenant le fichier compressé est réduite à chaque ligne spécifique à la table de correspondance caractere-occurrences (caractere et nombre d'occurrences) lue.
 * \note Cette fonction est destinée à être appelée lors de la décompression d'un fichier via Huffman.
 * \return Table de correspondance caractere-occurrences lue et créée (CodeHuffman*).
 * \callergraph
 * \callgraph
 */
CodeHuffman* lireCodesHuffman(uchar* contenuFichierReduit, int nombreLignesCodes);

/*! \fn Arbre *creerArbreDepuisEnTeteHuffman(EnTeteHuffman* eth)
 * \brief Lit un arbre depuis une en-tête.
 * \param [inout] eth (EnTeteHuffman*) : En-tête permettant de construire l'arbre de Huffman.
 * \details Cette fonction permet de construire un arbre de Huffman à partir d'une en-tête de Huffman lue depuis un fichier.
 * \note Cette fonction est destinée à être appelée lors de la décompression d'un fichier via Huffman.
 * \return Arbre "lu" et construit (Arbre*).
 * \callergraph
 * \callgraph
 */
Arbre *creerArbreDepuisEnTeteHuffman(EnTeteHuffman* eth);

#endif // ARBRE_H
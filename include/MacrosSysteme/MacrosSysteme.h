#ifndef MACROSSYSTEME_H
#define MACROSSYSTEME_H

/*! \page page2 MacrosSysteme
 * \par Description du fichier \a "MacrosSysteme.h" \a 
 * 		Le fichier \a "MacrosSysteme.h" \a inclut la totalité des fichiers définissant des macros ainsi que des fonctions
 * 		propres à différents systèmes d'exploitation ainsi qu'au C et au C++.
 */

/*! \file MacrosSysteme.h
 * \brief Inclut tout le nécessaire pour la portabilité d'un programme.
 * \details Ce fichier inclut la totalité des fichiers définissant des macros ainsi que des fonctions propres à différents
 * 			systèmes d'exploitation ainsi qu'au C et au C++. Les macros et fonctions définies dans les fichiers inclus ici
 * 			permettent d'offrir la portabilité entre les systèmes d'exploitation et entre le C et le C++.
 * \authors Joffrey PUJADE, Tom TISSERANT.
 * \date 12-12-2020.
 * \version 0.1
 */

#include "MacrosCharSpec.h"
#include "MacrosCmdOs.h"
#include "MacrosCouleur.h"

#endif // MACROSSYSTEME_H
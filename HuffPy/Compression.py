#coding: UTF-8

## @package Compression
# @brief Ce fichier regroupe les fonctions de compression d'archives.
# @details Les fonctions de ce fichier permettent la compression d'archives créées en utilisant la méthode de Huffman.
# @authors Joffrey PUJADE, Tom TISSERANT
# @date 10-12-2020
# @version 0.1

import os
import zipfile as zip
import subprocess
import HuffPy.ParcoursFichiers as pf
import HuffPy.SuppressionFichiers as sf

## @fn traitementFichiers(listeFichiers)
# @brief Traite les noms de fichiers.
# @param [in] listeFichiers (list) : Liste des fichiers à traiter.
# @details Cette fonction permet de créer une liste contenant, pour chaque maillon, une liste avec le nom du fichier source et
# 		   le nom du fichier de destination.
# @return Liste contenant les chemins et noms des fichiers source ainsi que les noms des fichiers de destination (list).
# @callergraph
# @callgraph
def traitementFichiers(listeFichiers) :
	listeRes = list()
	
	for fic in listeFichiers :
		tmp = fic.split('/')
		
		nomTmp = tmp[(len(tmp) - 1)]
		nom = (nomTmp.split('.')[0] + ".huffman")
		
		listTemp = list()
		listTemp.append(fic)
		listTemp.append(nom)
		
		listeRes.append(listTemp)
	
	return listeRes

## @fn compressionDeHuffman(listeFic)
# @brief Réalise la compression de Huffman.
# @param [in] listeFic (list) : Liste des fichiers à compresser avec la méthode de Huffman.
# @details Cette procédure permet de compresser les fichiers source avec la méthode de Huffman.
# @callergraph
# @callgraph
def compressionDeHuffman(listeFic) :
	FNULL = open(os.devnull, 'w') # Aucun affichage sur la sortie standard (flux "stdout").
	
	for fichier in listeFic :
		fichierSource = fichier[0]
		fichierDestination = fichier[1]
		
		arguments = "main -c " + fichierSource + " " + fichierDestination
		
		print("Début de la compression de \"" + fichierSource + "\" en \"" + fichierDestination + "\"...")
		
		subprocess.call(arguments, stdout = FNULL, stderr = FNULL, shell = False)
		
		print("Compression de \"" + fichierSource + "\" en \"" + fichierDestination + "\" terminée.")

## @fn ecrireInfosArchive(listeRep)()
# @brief Écrit les infos de l'archive.
# @param [in] listeRep (list) : Liste des sous-répertoires du répertoire à compresser.
# @details Cette procédure permet d'écrire le fichier "infosArchive", contenant les sous-répertoire du répertoire d'origine,
#		   du répertoire courant.
# @callergraph
# @callgraph
def ecrireInfosArchive(listeRep) :
	with open("infosArchive", "w") as fichierInfos :
		for i in range(len(listeRep)) :
			fichierInfos.write(listeRep[i])
			
			if(i < (len(listeRep) - 1)) :
				fichierInfos.write("\n")

## @fn uneFonctionPourLesArchiverTous(lesFichiers, listeRep, nomArchive)
# @brief Crée une archive.
# @param [in] lesFichiers (list) : Liste des fichiers à compresser avec la méthode de Huffman.
# @param [in] listeRep (list) : Liste des sous-répertoires du répertoire.
# @param [in] nomArchive (string) : Nom de l'archive à créer.
# @details Cette procédure permet de créer l'archive passée en paramètre.
# @callergraph
# @callgraph
def uneFonctionPourLesArchiverTous(lesFichiers, listeRep, nomArchive) :
	compressionDeHuffman(lesFichiers)
	ecrireInfosArchive(listeRep)
	
	with zip.ZipFile(nomArchive, 'w') as archive :
		for fichier in lesFichiers :
			archive.write(fichier[1])
		
		archive.write("infosArchive")
	
	sf.supprimerIntermediairesCompression(lesFichiers)

## @fn compression(fichierSource, fichierDestination)
# @brief Réalise la compression complète.
# @param [in] fichierSource (string) : Nom du répertoire à compresser.
# @param [in] fichierDestination (string) : Nom de l'archive à créer.
# @details Cette procédure permet de créer entièrement une archive avec la méthode de Huffman ainsi que de réaliser les
# 		   traitements adéquats à la fin de la compression de Huffman.
# @callergraph
# @callgraph
def compression(fichierSource, fichierDestination) :
	listeFichiers = pf.listeFichiers(fichierSource)
	listeFichiersConformes = traitementFichiers(listeFichiers)
	listeRep = pf.listeRepertoires(fichierSource)
	uneFonctionPourLesArchiverTous(listeFichiersConformes, listeRep, fichierDestination)

	for fichier in listeFichiers :
		print(fichier)
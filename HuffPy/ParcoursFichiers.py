#coding: UTF-8

## @package ParcoursFichiers
# @brief Ce fichier regroupe les fonctions de parcours de fichiers et de répertoires.
# @details La compression et la décompression des archives nécessitent le parcours de fichiers et de répertoires ; par conséquent, ces fonctions
# 		   sont là pour permettre les parcours de ceux-ci lorsque la compression et la décompression des archives l'exigent.
# @authors Joffrey PUJADE, Tom TISSERANT
# @date 10-12-2020
# @version 0.1

import os

## @fn listeFichiersRecursif(repertoire, listeFic)
# @brief Liste des fichiers de manière récursive.
# @param [in] repertoire (string) : Répertoire courant de la procédure.
# @param [in] listeFic (list) : Liste de fichiers en cours de création.
# @details Cette procédure récursive permet de lister tous les fichiers d'un répertoire donné.
# @callergraph
# @callgraph
def listeFichiersRecursif(repertoire, listeFic) :
	print("Je suis dans " + repertoire)
	liste = os.listdir(repertoire)
	
	for fichier in liste :
		if os.path.isdir(repertoire + "/" + fichier) :
			listeFichiersRecursif((repertoire + "/" + fichier), listeFic)
		else :
			listeFic.append((repertoire + "/" + fichier))

## @fn listeFichiers(repertoire)
# @brief Liste des fichiers.
# @param [in] repertoire (string) : Répertoire dont on doit lister les fichiers.
# @details Cette fonction permet de lister tous les fichiers d'un répertoire donné. C'est elle qui réalise le premier appel
# 		   de  la procédure listeFichiersRecursif(repertoire, listeFic).
# @return Liste des fichiers du répertoire (list).
# @callergraph
# @callgraph
def listeFichiers(repertoire) :
	listeFic = list()
	listeFichiersRecursif(repertoire, listeFic)
	return listeFic

## @fn listeRepertoiresRecursif(repertoire, listeRep)
# @brief Liste des répertoires de manière récursive.
# @param [in] repertoire (string) : Répertoire courant de la procédure.
# @param [in] listeRep (list) : Liste de répertoires en cours de création.
# @details Cette procédure récursive permet de lister tous les sous-répertoires d'un répertoire donné.
# @callergraph
# @callgraph
def listeRepertoiresRecursif(repertoire, listeRep) :
	print("Je suis dans " + repertoire)
	liste = os.listdir(repertoire)
	
	for rep in liste :
		if os.path.isdir(repertoire + "/" + rep) :
			listeRep.append((repertoire + "/" + rep))
			listeRepertoiresRecursif((repertoire + "/" + rep), listeRep)
		else :
			pass

## @fn listeRepertoires(repertoire)
# @brief Liste des répertoires.
# @param [in] repertoire (string) : Répertoire dont on doit lister les sous-répertoires.
# @details Cette fonction permet de lister tous les sous-répertoires d'un répertoire donné. C'est elle qui réalise le premier appel
# 		   de  la procédure listeRepertoiresRecursif(repertoire, listeRep).
# @return Liste des sous-répertoires du répertoire (list).
# @callergraph
# @callgraph
def listeRepertoires(repertoire) :
	listeRep = list()
	listeRepertoiresRecursif(repertoire, listeRep)
	return listeRep

## @fn listerFichiersHuffman(repertoire)
# @brief Liste des fichiers compressés.
# @param [in] repertoire (string) : Répertoire dont on doit lister les fichiers compressés.
# @details Cette fonction permet de lister tous les fichiers compressés d'un répertoire donné.
# @return Liste des fichiers compressés du répertoire (list).
# @callergraph
# @callgraph
def listerFichiersHuffman(repertoire) :
	listeTemp = os.listdir(repertoire)
	listeRes = list()
	
	for fic in listeTemp :
		if os.path.isfile(fic) :
			temp = os.path.splitext(fic)[1]
			
			if temp == ".huffman" :
				listeRes.append(fic)
	
	return listeRes
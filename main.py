#coding: UTF-8

import sys
import HuffPy.Compression as comp
import HuffPy.Decompression as decomp
import HuffPy.HuffExcept as he

def aide() :
	print("SYNOPSIS")
	print("	Cet exécutable permet la compression et la décompression d'un répertoire via")
	print("	l'algorithme de Huffman.")
	print("OPTIONS")
	print("	\"-h\", \"--help\"")
	print("		Affiche ce message d'aide.")
	print("	\"-c\", \"--compresser\" [répertoire] [archive]")
	print("		Permet la compression de [répertoire] passé en paramètre dans [archive], également passée en paramètre.")
	print("	\"-d\", \"--decompresser\" [archive]")
	print("		Permet la décompression de [archive] passée en paramètre dans le répertoire courant. Cette fonction")
	print("		recrée le répertoire tel qu'il était à l'origine.")

if sys.argv[1] == "-h" or sys.argv[1] == "--help" :
	aide()
elif sys.argv[1] == "-c" or sys.argv[1] == "--compresser" :
	if len(sys.argv) < 4 :
		print("ERREUR : Il manque soit le chemin du répertoire source, soit le nom de l'archive.")
	else :
		comp.compression(sys.argv[2], sys.argv[3])
elif sys.argv[1] == "-d" or sys.argv[1] == "--decompresser" :
	if len(sys.argv) < 3 :
		print("ERREUR : Il manque le nom de l'archive.")
	else :
		decomp.decompression(sys.argv[2])
else :
	print("ERREUR : Argument(s) invalide(s). Utilisez l'argument \"-h\" ou \"--help\" pour avoir de l'aide à l'utilisation.")
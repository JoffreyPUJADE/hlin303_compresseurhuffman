#include "../include/Manip.h"

uchar* allouerChaine(int tailleAlloc)
{
	uchar* chaine = ((uchar*)malloc((sizeof(uchar) * tailleAlloc)));
	
	if(chaine != NULL)
		return chaine;
	
	definirCouleurConsole(ROUGE);
	
	perror("ERREUR ALLOCATION CHAINE\n");
	
	definirCouleurConsole(NORMAL);
	
	return NULL;
}

int compterCarFic(char const* nomFichier)
{
	FILE* fichier = fopen(nomFichier, "r");
	
	if(fichier != NULL)
	{
		int compteur = 0;
		uchar c;
		
		while(!feof(fichier))
		{
			size_t valeurTampon = fread(&c, 1, 1, fichier);
			++valeurTampon;
			
			if(compteur < INT_MAX)
			{
				compteur++;
			}
			else
			{
				definirCouleurConsole(ROUGE);
				
				perror("ERREUR COMPTEUR CAR FIC 2\n");
				
				definirCouleurConsole(NORMAL);
				
				fclose(fichier);
				
				exit(1);
			}
		}
		
		return compteur;
	}
	else
	{
		definirCouleurConsole(ROUGE);
		
		perror("ERREUR COMPTEUR CAR FIC 2\n");
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	fclose(fichier);
	
	return -1;
}

uchar* fichierVersChaine(char const* nomFichier, int* tailleChaine)
{
	FILE* fichier = fopen(nomFichier,"r");
	
	if((fichier != NULL) && (tailleChaine != NULL))
	{
		*tailleChaine = compterCarFic(nomFichier);
		uchar* chaine = allouerChaine(*tailleChaine);
		
		uchar c;
		int offset = 0;
		
		while(!feof(fichier))
        {
			size_t valeurTampon = fread(&c, 1, 1, fichier);
			++valeurTampon;
			
            chaine[offset] = c;
            offset++;
        }
		
		return chaine;
	}
	else
	{
		definirCouleurConsole(ROUGE);
		
		perror("ERREUR DANS fichierVersChaine\n");
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	fclose(fichier);
	
	return NULL;
}

int estVide(char const* nomFichier)
{
	FILE* fichier = fopen(nomFichier, "r");
	
	if(fichier == NULL)
	{
		definirCouleurConsole(ROUGE);
		
		fprintf(stderr, "Impossible d'ouvrir le fichier \"%s\" en mode lecture.\n", nomFichier);
		
		definirCouleurConsole(NORMAL);
		
		exit(1);
	}
	
	return (fgetc(fichier) == EOF);
}